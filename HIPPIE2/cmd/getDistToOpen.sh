

cat out/SRR*_1_*_interRS.txt|awk 'BEGIN{OFS="\t"} \
	{if ($2=="u"){split ($1,a,":");data[a[1]"\t"a[2]-1"\t"a[2]]++} \
	 if($4=="u"){split ($3,a,":");data[a[1]"\t"a[2]-1"\t"a[2]]++}}END{for (i in data) print i, data[i]}' > out/hic003_readsToUpRS.bed

awk '{ print >> $1 "_hic003_readsToUpRS.bed" }' out/hic003_readsToUpRS.bed

for i in out/*_hic003_readsToUpRS.bed; 
	do prefix=`echo $i| cut -d'_' -f 1`;
	awk 'BEGIN{OFS="\t"}{a[$0]++} END {for (i in a) print i,a[i]}' $i| \
	sort -T /project/wang2/temp -k1,1 -k2,2n $i > out/${prefix}_hic003_readsToUpRS_sorted.bed;
done;



