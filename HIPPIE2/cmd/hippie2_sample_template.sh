#!/bin/bash

#
# Initialization
export HIPPIE_INI=
export HIPPIE_CFG=
source $HIPPIE_INI
source $HIPPIE_CFG

# get command line parameters
while getopts dr:l:p:t: option
do
        case "$option" in
            d) DODEBUG=true;;               # option -d to print out commands
            r) RUN_DIR="$OPTARG";;
            l) LINE="$OPTARG";;
            p)                              # -p 1/2/3 to execute a particular phase, e.g.: -p1 -p2 -p3
              case "$OPTARG" in
                1) DOPHASE1=true;;
                2) DOPHASE2=true;;
                3) DOPHASE3=true;;
              esac;;
            t) TEST_FUNCTION="$OPTARG";;    # execute function e.g.,   -t "stat _merged_nodup_uniq_recal"
        esac
done

#
# Globals
USER=$LOGNAME
## don't need this export value
## NOTE: for some reason, it doesn't work unless I comment this out..
# export CMD_DIR=$CMD_HIPPIE
export REF_FASTA=$GENOME_REF
export SAMPLE_DIR=
export FASTQ_DIR=
export SAM_DIR=
export BED_DIR=
export OUT_DIR=
#
# User Configuration


USESEG=0	# USESEG=0: Enhancers specified by covering H3K4me1 or K27ac and DNase, but not H3K27me3 or H3K4me3
					# USESEG=1: Enhancers specified by enhancers and weak enhancers from ENCODE Combined Segmentations tracks



#Template variables below
#export RG_STR=

export RGID=
export LINE=

export CELL=
export RE=

export mbq=
export mmq=

tilesSRR=($(ls $FASTQ_DIR/*.fastq*|sed -n 's/.*\(SRR[0-9]\+_1_[0-9]\+.*\)/\1/p')) # this extracts a different format
#tilesSRR=($(ls $FASTQ_DIR/*.fastq*|sed -n 's/.*\(SRR[0-9]\+_1\).*/\1/p')) # this extracts a different format

#
# QSUB
export BSUB="bsub"
[[ $DODEBUG ]] && BSUB="echo bsub"

#export QSUBARGS="-S /bin/bash -q $GRID_QUEUE -cwd -v HIPPIE_INI,HIPPIE_CFG,REF_FASTA -j y -o $STDOUT -hard -l h_stack=256m,h_vmem=1G"

#
# function libraries
source $CMD_DIR/lib/hippie2.phase1_lib.pe.sh

if [ ! -z "$TEST_FUNCTION" ]
then
 eval $TEST_FUNCTION
 exit
fi

#############################################################
# PHASE 1 - Create BAM alignments                           #

if [[ $DOPHASE1 ]]; then
  echo "Phase 1"
	loadGenome
	starMapping
	pairingBam
	direction
	assignRS
fi
#	direction_check
	#assignToOpen

###################### EOF PHASE 1 #########################
