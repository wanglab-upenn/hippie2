#!/bin/bash
#
#BSUB -J HIC014sra_toFastq          # job name
#BSUB -o /home/yihhwang/stdout/sra_toFastq.%J     # output file name in which %J is replaced by the job ID
#BSUB -q wang_normal

/home/yihhwang/bin/sratoolkit.2.4.2-centos_linux64/bin/fastq-dump --split-3 ./SRR1658585.sra
/home/yihhwang/bin/sratoolkit.2.4.2-centos_linux64/bin/fastq-dump --split-3 ./SRR1658586.sra

