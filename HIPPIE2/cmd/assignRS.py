import sys, re, time

re_file = sys.argv[1]	# restriction site (MboI: 4bp)
input_file = sys.argv[2]	# bed file with directionality identified
out_file = sys.argv[3]	# output file name

def restriction_site_reader(in_file):
	resite_start = dict()
	resite_end = dict()
	for l in open(re_file):
		v = l.rstrip().split()
		if v[0] not in resite_start:
			resite_start[v[0]] = list()
			resite_end[v[0]] = list()
		resite_start[v[0]].append(int(v[1]))
		resite_end[v[0]].append(int(v[2]))
	return resite_start, resite_end

def sort_by_coord (idx1, idx2):
	v1 = re.split('\t',idx1)
	v2 = re.split('\t',idx2)
	if v1[0] == v2[0]:
		if int(v1[1])==int(v2[1]):
			if v1[3]=="u" and v2[3]=="d":
				return idx1, idx2
			elif v1[3]=="d" and v2[3]=="u":
				return idx2, idx1
			elif v1[3] == "u" and v1[3] =="u":
				return idx1, idx2
			else:
				return idx2, idx1
		else:
			return (idx1,idx2) if (int(v1[1]) < int(v2[1])) else (idx2,idx1)
	else:
		firstChr = v1[0].replace("chr", "")
		sndChr = v2[0].replace("chr", "")
		if(firstChr=="X"):
			firstChr="23"
		elif(firstChr=="Y"):
			firstChr="24"
		if(sndChr == "X"):
			sndChr="23"
		elif(sndChr=="Y"):
			sndChr="24"
		return (idx1, idx2) if int(firstChr) < int(sndChr) else (idx2, idx1)


def alignment_reader(in_file,  out_file, resite_start, resite_end):
	f1 = open(in_file)
#chr4    703268  703369  -       23      2098    d       chr4    702918  703019  -       204     222     d       SRR1658576.7  .       good    chr4_1766,1768  chr4_1765,1766
	fout = open(out_file, 'w')
	for line in f1:
		v = line.rstrip().split("\t")
		#if the directionality can be determined:
		if v[16] == "good" or v[16] == "goodC":
			reindex1 = re.split('_|,',v[17])
			reindex2 = re.split('_|,',v[18])
			index1="."
			index2="."

			if v[6] == "u":
				index1 = reindex1[0]+"\t"+str(resite_start[reindex1[0]][int(reindex1[2])])+"\t"+str(resite_end[reindex1[0]][int(reindex1[2])])+"\t"+v[6]
			elif v[6] == "d":
				index1 = reindex1[0]+"\t"+str(resite_start[reindex1[0]][int(reindex1[1])])+"\t"+str(resite_end[reindex1[0]][int(reindex1[1])])+"\t"+v[6]
			else:
				index1 = "."

			if v[13] == "u":
				index2 = reindex2[0]+"\t"+str(resite_start[reindex2[0]][int(reindex2[2])])+"\t"+str(resite_end[reindex2[0]][int(reindex2[2])])+"\t"+v[13]
			elif v[13] == "d":
				index2 = reindex2[0]+"\t"+str(resite_start[reindex2[0]][int(reindex2[1])])+"\t"+str(resite_end[reindex2[0]][int(reindex2[1])])+"\t"+v[13]
			else:
				index2 = "."

			if index1 == "." or index2 == ".":
				continue

			else:
				index1, index2 = sort_by_coord (index1, index2)
			fout.write("\t".join([str (s) for s in [index1,  index2, v[14],v[15],v[16]]])+"\n")
	f1.close()
	fout.close()


sys.stdout.write("["+time.strftime("%c")+"] Start assigning read to restriction site... " +"\n")
sys.stdout.flush()

sys.stdout.write("["+time.strftime("%c")+"] Reading restriction site file... "+"\n")
sys.stdout.flush()
start = time.clock()
re_start, re_end = restriction_site_reader(re_file)
sys.stdout.write("Time reading restriction site file: "+ str(time.clock()-start)+ " s"+"\n")
sys.stdout.flush()


sys.stdout.write("["+time.strftime("%c")+"] Reading direction bed read file... " +"\n")
sys.stdout.flush()

start = time.clock()
alignment_reader(input_file,  out_file, re_start, re_end)
sys.stdout.write("Time reading direction bed file: "+ str(time.clock()-start)+ " s"+"\n")
sys.stdout.flush()

sys.stdout.write( "["+time.strftime("%c")+"] Complete analysis (assign_element) successfully "+"\n")
sys.stdout.flush()

