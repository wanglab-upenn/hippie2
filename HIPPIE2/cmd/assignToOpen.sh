
printf "%s\n" "[$(date)] Start assinging reads to closest DNAse footprint..."
printf "%s\n" "[$(date)] Merging all interating RS pairs into one file..."

cat ${OUT_DIR}/SRR*_1_*_interRS.txt|awk 'BEGIN{OFS="\t"} \
  {if ($2=="u"){split ($1,a,":");data[a[1]"\t"a[2]-1"\t"a[2]]++} \
   if($4=="u"){split ($3,a,":");data[a[1]"\t"a[2]-1"\t"a[2]]++}}END{for (i in data) print i, data[i]}' > ${OUT_DIR}/${LINE}_readsToUpRS.bed

cat ${OUT_DIR}/SRR*_1_*_interRS.txt|awk 'BEGIN{OFS="\t"} \
  {if ($2=="d"){split ($1,a,":");data[a[1]"\t"a[2]"\t"a[2]+1]++} \
   if($4=="d"){split ($3,a,":");data[a[1]"\t"a[2]"\t"a[2]+1]++}}END{for (i in data) print i, data[i]}' > ${OUT_DIR}/${LINE}_readsToDownRS.bed

printf "%s\n" "[$(date)] Splitting the RS sites (with HIC reads) by chromosomes..."

awk -v outdir=${OUT_DIR} -v line=${LINE} '{ print >> outdir "/" $1 "_" line "_readsToUpRS.bed" }' ${OUT_DIR}/${LINE}_readsToUpRS.bed
awk -v outdir=${OUT_DIR} -v line=${LINE} '{ print >> outdir "/" $1 "_" line "_readsToDownRS.bed" }' ${OUT_DIR}/${LINE}_readsToDownRS.bed

printf "%s\n" "[$(date)] Sorting the HIC RS sites by coordinates for each chromosome..."

tiles=($(ls $OUT_DIR/*_${LINE}_readsToUpRS.bed))
for i in "${tiles[@]}"; do 
	name=`basename $i`
	prefix=`echo $name| cut -d'_' -f 1`;
  awk 'BEGIN{OFS="\t"}{a[$0]++} END {for (i in a) print i,a[i]}' $i| \
  sort -T ${TMPDIR} -k1,1 -k2,2n $i > ${OUT_DIR}/${prefix}_${LINE}_readsToUpRS_sorted.bed;
done;

tiles=($(ls $OUT_DIR/*_${LINE}_readsToDownRS.bed))
for i in "${tiles[@]}"; do 
	name=`basename $i`
	prefix=`echo $name| cut -d'_' -f 1`;
  awk 'BEGIN{OFS="\t"}{a[$0]++} END {for (i in a) print i,a[i]}' $i| \
  sort -T ${TMPDIR} -k1,1 -k2,2n $i > ${OUT_DIR}/${prefix}_${LINE}_readsToDownRS_sorted.bed;
done;

printf "%s\n" "[$(date)] Complete assinging RS to open region (assignToOpen) successfully"

exit $?

