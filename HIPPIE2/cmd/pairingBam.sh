#!/bin/bash
#
 
#export PYTHON="/appl/python-2.76/bin/python2.7"
export bam_file=$1
export sec_bam_file=$2
export chim_file=$3
export sec_chim_file=$4
export ChimSegMin=$5
export size_select=$6
export re_file=$7
export out=$8



$PYTHON ${CMD_DIR}/parse_starBam_to_paired.py $bam_file $sec_bam_file $chim_file $sec_chim_file $ChimSegMin $size_select $re_file $out


# #Error checking
# LSF_STDOUT_PATH=${STDOUT_DIR}${LSB_JOBNAME}".o"${LSB_JOBID}
# PROCESSED=$(grep -i "error" $LSF_STDOUT_PATH |tail -n 1)

# shopt -s nocasematch
# echo "checking stdout file: " $LSF_STDOUT_PATH
# if [[ $PROCESSED =~ "error" ]];
#         then
#                 echo $PROCESSED
#                 exit 100
# fi

