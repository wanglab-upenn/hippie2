# export fastq_file=$1
# export fastq_file_prefix=${fastq_file%%.*}
#export RE_SITE="/home/yihhwang/ET_REF/hg19/chromosomes/MboI_site.bed"

#tilesSRR=($(ls $BED_DIR/*.bed|sed -n 's/.*\(SRR[0-9]\+_1_[0-9]\+\).bed/\1/p')) # this extracts bed files
tilesSRR=($(ls $FASTQ_DIR/*.fastq*|sed -n 's/.*\(SRR[0-9]\+_1_[0-9]\+\).fastq.*/\1/p'))


for task in "${tilesSRR[@]}"; do
    echo $task;
    bed_file=${task}".bed";
    JOBNAME="${task}_direction" ;
    echo "$BSUB -J ${JOBNAME} -q wang_normal -e ${STDERR_DIR}/${JOBNAME}.e%J -o ${STDOUT_DIR}/${JOBNAME}.o%J -M 1024 -m \"${BSUB_NODES}\"  ${CMD_DIR}/get_direction_check.sh ${RE_SITE} ${BED_DIR}/${bed_file} ${OUT_DIR}/${task}_direction.txt;"
    $BSUB -J ${JOBNAME} -q wang_normal -e ${STDOUT_DIR}/${JOBNAME}.e%J -o ${STDOUT_DIR}/${JOBNAME}.o%J -M 1024 -m "${BSUB_NODES}" ${CMD_DIR}/get_direction_check.sh ${RE_SITE} ${BED_DIR}/${bed_file} ${OUT_DIR}/${task}_direction.txt;
done


# 5 * 1024 MB = 5120 = 5 GB
# 10 * 1024 MB = 10240 = 10 GB
# 10 * 1024 * 1024 MB = 10485760 = 10 TB



