# export fastq_file=$1
# export fastq_file_prefix=${fastq_file%%.*}
# export JOBNAME="${fastq_file_prefix}_mapping"
# export RE_SITE="/home/yihhwang/ET_REF/hg19/chromosomes/MboI_site.bed"

#tilesSRR=($(ls $FASTQ_DIR/*.fastq*|sed -n 's/.*\(SRR[0-9]\+_1_[0-9]\+\).fastq.*/\1/p'))
tilesSRR=($(ls $FASTQ_DIR/*.fastq*|sed -n 's/.*\(SRR[0-9]\+_1_[0-9]\+.*\)/\1/p'))

for task in "${tilesSRR[@]}"; do
    echo $task;
    export fastq_file=${task};
    export fastq_file_prefix=${fastq_file%%.*};
    export task=${fastq_file_prefix}

    bam_file=${task}"Aligned.out.bam";
    chim_file=${task}"Chimeric.out.sam" 
    
    sec_bam_file=$(echo $bam_file|sed 's/_1_/_2_/');
    sec_chim_file=$(echo $chim_file|sed 's/_1_/_2_/');
    JOBNAME="${task}_pairing" ;
    sec_task=$(echo $task|sed 's/_1_/_2_/');
    echo "$BSUB -J ${JOBNAME} -n 1 -w \"done(${sec_task}_mapping)&&done(${task}_mapping)\" -q wang_normal -e ${STDERR_DIR}/${JOBNAME}.e%J -o ${STDOUT_DIR}/${JOBNAME}.o%J -M 16000 -m \"${BSUB_NODES}\" ${CMD_DIR}/pairingBam.sh ${SAM_DIR}/${bam_file} ${SAM_DIR}/${sec_bam_file} ${SAM_DIR}/${chim_file} ${SAM_DIR}/${sec_chim_file} ${ChimSegMin} ${SIZE_SELECT} ${RE_SITE} ${BED_DIR}/${task}.bed;"
    $BSUB -J ${JOBNAME} -n 1 -w "done(${sec_task}_mapping)&&done(${task}_mapping)" -q wang_normal -e ${STDERR_DIR}/${JOBNAME}.e%J -o ${STDOUT_DIR}/${JOBNAME}.o%J -M 16000 -m "${BSUB_NODES}" ${CMD_DIR}/pairingBam.sh ${SAM_DIR}/${bam_file} ${SAM_DIR}/${sec_bam_file} ${SAM_DIR}/${chim_file} ${SAM_DIR}/${sec_chim_file} ${ChimSegMin} ${SIZE_SELECT} ${RE_SITE} ${BED_DIR}/${task}.bed;
done

# 5 * 1024 MB = 5120 = 5 GB
# 10 * 1024 MB = 10240 = 10 GB
# 10 * 1024 * 1024 MB = 10485760 = 10 TB


