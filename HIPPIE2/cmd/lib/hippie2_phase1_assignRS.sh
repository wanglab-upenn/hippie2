
tilesSRR=($(ls $FASTQ_DIR/*.fastq*|sed -n 's/.*\(SRR[0-9]\+_1_[0-9]\+\).fastq.*/\1/p'))

for task in "${tilesSRR[@]}"; do
    echo $task;
    JOBNAME="${LINE}_${task}_assignRS" ;
    echo "$BSUB -J ${JOBNAME} -n1 -w \"done(${task}_direction)\" -q wang_normal -e ${STDERR_DIR}/${JOBNAME}.e%J -o ${STDOUT_DIR}/${JOBNAME}.o%J -M 1024 -m \"${BSUB_NODES}\"  ${CMD_DIR}/assignRS.sh ${RE_SITE} ${OUT_DIR}/${task}_direction.txt ${OUT_DIR}/${task}_interRS.txt;"
    $BSUB -J ${JOBNAME} -n1 -w "done(${task}_direction)" -q wang_normal -e ${STDERR_DIR}/${JOBNAME}.e%J -o ${STDOUT_DIR}/${JOBNAME}.o%J -M 1024 -m "${BSUB_NODES}" ${CMD_DIR}/assignRS.sh ${RE_SITE} ${OUT_DIR}/${task}_direction.txt ${OUT_DIR}/${task}_interRS.txt;
    #	$BSUB -J ${JOBNAME} -n1 -q wang_normal -o ${STDOUT_DIR}/${JOBNAME}.o%J -M 1024 ${CMD_DIR}/assignRS.sh ${RE_SITE} ${OUT_DIR}/${task}_direction.txt ${OUT_DIR}/${task}_interRS.txt;
done


# 3 * 1024 MB = 3072 = 3GB
