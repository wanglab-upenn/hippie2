
export STDOUT_DIR="/home/yihhwang/stdout"
export CMD_DIR="/home/yihhwang/CELL/HIC003"
export SAMPLE_DIR="/home/yihhwang/CELL/HIC014"
export BSUB="bsub"


export JOBNAME="rm_hg19_MboI_genome_chaffee"
$BSUB -J ${JOBNAME} \
 -q wang_normal \
 -o ${STDOUT_DIR}/${JOBNAME}.o%J \
 -M 10485760 \
 -m chaffee \
 ${CMD_DIR}/rmGenome.sh chaffee ${SAMPLE_DIR}

export JOBNAME="rm_hg19_MboI_genome_maxwell"
$BSUB -J ${JOBNAME} \
 -q wang_normal \
 -o ${STDOUT_DIR}/${JOBNAME}.o%J \
 -M 10485760 \
 -m maxwell \
 ${CMD_DIR}/rmGenome.sh maxwell ${SAMPLE_DIR}


# 5 * 1024 MB = 5120 = 5 GB
# 10 * 1024 MB = 10240 = 10 GB
# 10 * 1024 * 1024 MB = 10485760 = 10 TB


