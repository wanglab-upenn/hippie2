export BSUB="bsub"

tilesSRR=($(ls $FASTQ_DIR/*.fastq*|sed -n 's/.*\(SRR[0-9]\+_1_[0-9]\+.*\)/\1/p')) # this extracts a different format

for task in "${tilesSRR[@]}"; do
    echo $task;
    export fastq_file=${task};
    export fastq_file_prefix=${fastq_file%%.*};   
    export JOBNAME="${fastq_file_prefix}_mapping" ;
    ## set up the bsub waiting string
    BSUB_NODE_ARR=($BSUB_NODES)
    BSUB_WAIT_STRING="done(load_hg19_MboI_genome_${BSUB_NODE_ARR[0]})"
    for i in ${BSUB_NODE_ARR[@]:1}; do
	BSUB_WAIT_STRING="done(load_hg19_MboI_genome_$i) && ${BSUB_WAIT_STRING}"
    done    

    ## NOTE: removed all "-n 1" arguments (after the -J argument), seem to be incompatible with bsub
    echo "$BSUB -J ${JOBNAME} -w \"${BSUB_WAIT_STRING}\" -q wang_normal -e ${STDERR_DIR}/${JOBNAME}.e%J -o ${STDOUT_DIR}/${JOBNAME}.o%J -m \"${BSUB_NODES}\" -M 30000 ${CMD_DIR}/starMappingToBam.sh ${FASTQ_DIR}/${fastq_file} ${SAM_DIR} ${ChimSegMin};"
    ## -n 1
    $BSUB -J ${JOBNAME} -w "${BSUB_WAIT_STRING}" -q wang_normal -e ${STDERR_DIR}/${JOBNAME}.e%J -o ${STDOUT_DIR}/${JOBNAME}.o%J -m "${BSUB_NODES}" -M 30000 ${CMD_DIR}/starMappingToBam.sh ${FASTQ_DIR}/${fastq_file} ${SAM_DIR} ${ChimSegMin};
    sec_fastq_file=$(echo $fastq_file|sed 's/_1_/_2_/');
    export sec_fastq_file_prefix=${sec_fastq_file%%.*};
    export sec_JOBNAME="${sec_fastq_file_prefix}_mapping" ;
    echo "$BSUB -J ${sec_JOBNAME} -w \"${BSUB_WAIT_STRING}\" -q wang_normal -e ${STDERR_DIR}/${sec_JOBNAME}.e%J -o ${STDOUT_DIR}/${sec_JOBNAME}.o%J -m \"${BSUB_NODES}\" -M 30000 ${CMD_DIR}/starMappingToBam.sh ${FASTQ_DIR}/${sec_fastq_file} ${SAM_DIR} ${ChimSegMin};"
    ## -n 1 
    $BSUB -J ${sec_JOBNAME} -w "${BSUB_WAIT_STRING}" -q wang_normal -e ${STDERR_DIR}/${sec_JOBNAME}.e%J -o ${STDOUT_DIR}/${sec_JOBNAME}.o%J -m "${BSUB_NODES}" -M 30000 ${CMD_DIR}/starMappingToBam.sh ${FASTQ_DIR}/${sec_fastq_file} ${SAM_DIR} ${ChimSegMin};
done

