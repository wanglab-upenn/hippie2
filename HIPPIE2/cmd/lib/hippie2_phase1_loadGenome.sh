#!/bin/bash

## loop through the nodes we want to use
for NODE in ${BSUB_NODES}; do 
    export JOBNAME="load_hg19_MboI_genome_${NODE}"

    $BSUB -J ${JOBNAME} \
	-q wang_normal \
	-o ${STDOUT_DIR}/${JOBNAME}.o%J \
	-e ${STDERR_DIR}/${JOBNAME}.e%J \
	-M 51200 \
	-m "${NODE}" \
	${CMD_DIR}/loadGenome.sh ${NODE} ${SAMPLE_DIR}
done
    
# 5 * 1024 MB = 5120 = 5 GB
# 10 * 1024 MB = 10240 = 10 GB
# 50 * 1024 MB = 51200 = 50 GB
# 10 * 1024 * 1024 MB = 10485760 = 10 TB


