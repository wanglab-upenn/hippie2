#!/bin/bash

## hippie2.phase1_lib.pe.sh
## this is a function library loaded in phase 1 Hi-C analysis
## it relies on the CMD_DIR variable being defined

function loadGenome {
    echo -e "Submitting loadGenome"
    sh ${CMD_DIR}/lib/hippie2_phase1_loadGenome.sh
}

function starMapping {
    echo -e "Submitting StarMapping"
    sh ${CMD_DIR}/lib/hippie2_phase1_starMapping.sh
}

function pairingBam {
    echo -e "Submitting pairingBam"
    sh ${CMD_DIR}/lib/hippie2_phase1_pairingBam.sh
}

function direction {
    echo -e "Submitting direction"
    sh ${CMD_DIR}/lib/hippie2_phase1_direction.sh
}

#
function direction_check {
    echo -e "Submitting direction_check"
    sh ${CMD_DIR}/lib/hippie2_phase1_direction_check.sh
}
#

function assignRS {
    echo -e "Submitting assignRS"
    sh ${CMD_DIR}/lib/hippie2_phase1_assignRS.sh
}

function assignToOpen {
    echo -e "Submitting assignToOpen"
    sh ${CMD_DIR}/lib/hippie2_phase1_assignToOpen.sh
}
#####
#
#
####
function hippie_test_variables {

# execFiles[0]=BWA
# execFiles[1]=GATK
# execFiles[2]=SAMTOOLS

 local execFiles=(BWA GATK SAMTOOLS)
 local dataFiles=(REF_FASTA SNPDB SNPDB_INDELS_ONLY TARGET FLANK)
 local fileDirs=(GENOMICS_JAR GENOMICS_BIN CMD_HIPPIE STDOUT CMD_DIR BUNDLE_HAPMAP BUNDLE_OMNI BUNDLE_MILLS)

 local MISSING_SOMETING=0
 local DO_CONTINUE=1 # 1= all clear, 0=fatal error, 2=warning

 if [ ! -z "$1" ];then
  echo "Testing your variable"
 else

    # Testing directories exists
    for VAR in ${fileDirs[@]};do

        if [ -z $(eval echo $`echo $VAR`) ];then
          echo "Variable $VAR is not set"
        fi

        if [ ! -d $(eval echo $`echo $VAR`) ];then
            echo "$""$VAR can not be found, missing" $(eval echo $`echo $VAR`)
            MISSING_SOMETING=1
            DO_CONTINUE=2
        fi
    done

    # Testing programs can be found
    for VAR in ${execFiles[@]};do

        if [ -z $(eval echo $`echo $VAR`) ];then
          echo "Variable $VAR is not set"
        fi

        if [ ! -e $(eval echo $`echo $VAR`) ];then
            echo "$""$VAR program can not be found, missing" $(eval echo $`echo $VAR`)
            MISSING_SOMETING=1
            DO_CONTINUE=0
        fi
    done

    # Testing data files exists
    for VAR in ${dataFiles[@]};do

        if [ -z $(eval echo $`echo $VAR`) ];then
          echo "Variable $VAR is not set"
        fi

        if [ ! -s $(eval echo $`echo $VAR`) ];then
            echo "$""$VAR data file can not be found, missing" $(eval echo $`echo $VAR`)
            MISSING_SOMETING=1
            DO_CONTINUE=0
        fi
    done

 fi

 if [ $MISSING_SOMETING -eq 1 ];then
    echo "Please correct your configuration files:" $HIPPIE_INI $HIPPIE_CFG
 fi

 eval "hippie_test_variables=$DO_CONTINUE"

}
