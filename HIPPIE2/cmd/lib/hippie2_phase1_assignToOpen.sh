

tilesSRR=($(ls $FASTQ_DIR/*.fastq*|sed -n 's/.*\(SRR[0-9]\+_1_[0-9]\+\).fastq.*/\1/p'))

assignRSJN=()
for task in "${tilesSRR[@]}"; do
  assignRSJN+=("done("${LINE}"_"$task"_assignRS)")
done;

sep=" && "
depend="$( printf "${sep}%s" "${assignRSJN[@]}" )"
depend="${depend:${#sep}}" # remove leading separator

JOBNAME="${LINE}_assignToOpen" ;
echo "$BSUB -J ${JOBNAME} -n1 -w \"$depend\" -q wang_normal -o ${STDOUT_DIR}/${JOBNAME}.o%J -M 3072 -m \"${BSUB_NODES}\" ${CMD_DIR}/assignToOpen.sh;"
$BSUB -J ${JOBNAME} -n1 -w "$depend" -q wang_normal -o ${STDOUT_DIR}/${JOBNAME}.o%J -M 3072 -m "${BSUB_NODES}" ${CMD_DIR}/assignToOpen.sh;

# 3 * 1024 MB = 3072 = 3GB 


