#!/bin/bash
#
 
export STAR="/home/yihhwang/bin/STAR2.4.0h/bin/Linux_x86_64_static/STAR"
export REF_FASTA="/home/yihhwang/ET_REF/hg19/bigZips_RESiteFlanking500_star-2.4.0h"
export FLOWCELLPATH="/home/yihhwang/CELL"
export fastq_file=$1
export SAM_DIR=$2
export sam_file_prefix="${SAM_DIR}/$(basename ${fastq_file%.*})"
# export fastq_file_prefix=${fastq_file%%.*}
#export STAR_THREADS=2  # thread has to be 1 to keep the read order

$STAR \
 --runThreadN 1 \
 --genomeDir "${REF_FASTA}/" \
 --readFilesIn "${fastq_file}" \
 --outSAMattributes All \
 --outSAMunmapped Within \
 --outReadsUnmapped Fastx \
 --outFileNamePrefix "${sam_file_prefix}" \
 --outFilterMultimapNmax 1   \
 --outFilterMismatchNoverLmax 0.04 \
 --scoreGapNoncan 0  --scoreGapGCAG 0  --scoreGapATAC 0 \
 --alignIntronMax 1 \
 --chimSegmentMin 20 \
 --chimScoreJunctionNonGTAG 0 \
 --genomeLoad LoadAndKeep 
# segmentmin=20 actually means 22M79S and 22S79M in STAR (as they might keep 2 nt for splicesite)
# --runThreadN $STAR_THREADS \

