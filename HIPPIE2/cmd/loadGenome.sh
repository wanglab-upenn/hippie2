#!/bin/bash
source $HIPPIE_INI
source $HIPPIE_CFG

export STAR_THREADS=2
export NODE_ID=$1
export DATA_DIR=$2


${STAR} \
 --runThreadN ${STAR_THREADS} \
 --genomeDir "${REF_FASTA}" \
 --outFileNamePrefix "${DATA_DIR}/${NODE_ID}_loadGenome" \
 --genomeLoad LoadAndExit

rm "${DATA_DIR}/${NODE_ID}_loadGenomeLog.progress.out"
rm "${DATA_DIR}/${NODE_ID}_loadGenomeAligned.out.sam"
rm -rf "${DATA_DIR}/${NODE_ID}_loadGenome_STARtmp"

# this error checking doesn't work because the output file isn't created yet..
# LSF_STDOUT_PATH=${STDOUT_DIR}${LSB_JOBNAME}".o"${LSB_JOBID}
# PROCESSED=$(grep "FATAL ERROR" $LSF_STDOUT_PATH |tail -n 1)

# echo "checking stdout file: " $LSF_STDOUT_PATH
# if [[ $PROCESSED =~ "FATAL ERROR" ]];
#   then
#     echo $PROCESSED
#     exit 100
# fi
