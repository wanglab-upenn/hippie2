#!/bin/bash
#
#BSUB -J HIC014splitting_fastq          # job name
#BSUB -o /home/yihhwang/stdout/splitting_fastq.%J     # output file name in which %J is replaced by the job ID
#BSUB -q wang_normal
#BSUB -n 8
#BSUB -w "done(HIC014sra_toFastq)"

export NUM_LANES=80000000 # 20M of reads for each file
export CPU_NUM=8

mkdir fastq; mkdir sam;mkdir bed;mkdir out
tilesSRR=($(ls ./*.fastq|sed -n 's/.*\(SRR[0-9]\+.*\)/\1/p')) # this extracts a different format

for file in "${tilesSRR[@]}"; do
	echo "$(date) splitting" ${file}; 
	split -l ${NUM_LANES} ${file} ${file%%.*}_ -a4 -d;
	for i in ${file%%.*}_*; do
		/home/yihhwang/bin/pigz-2.3.1/pigz -9c -p ${CPU_NUM} ${i} > fastq/${i}.fastq.gz;
	#	gzip -9c ${i} > fastq/${i}.fastq.gz;
	done;
done;


echo "$(date) done splitting ${file}"
