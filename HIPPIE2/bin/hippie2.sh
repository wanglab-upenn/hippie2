#!/bin/bash
# hippie2.sh - A high-throughput identification pipeline for promoter interacting enhancer elements using Hi-C data
# Comments: Yih-Chii Hwang<yihhwang@mail.med.upenn.edu>
#######################################################################################################

#
# get command line parameters
while getopts h:f:i:mbp:t: option
do
        case "$option" in
            h) HIPPIE_HOME_DIR="$OPTARG";;  # option -h path to hippie_home
            f) CONFIG="$OPTARG";;         # option -f path to config file
            i) HIPPIE_INI="$OPTARG";;               # option -i specify INI file
#            b) BATCHONLY=true;;           # option -b if run in batch mode only
            p)                            # -p 1/2/3 to execute a particular phase, e.g.: -p1 -p2 -p3
              case "$OPTARG" in
                1) DOPHASE1=true;;
                2) DOPHASE2=true;;
                3) DOPHASE3=true;;
              esac;;
        esac
done

if [ -z "$HIPPIE_HOME" ];then
  echo "Missing HIPPIE_HOME:$HIPPIE_HOME"
  if [ ! -z "$HIPPIE_HOME_DIR" ];then
    echo "Loading HIPPIE_HOME_DIR:$HIPPIE_HOME_DIR"
    export HIPPIE_HOME=$HIPPIE_HOME_DIR
  fi
fi
#
# Initialization
export PWD=`pwd`

if [ -z "$HIPPIE_INI" ];then
  export HIPPIE_INI=$HIPPIE_HOME/hippie2.ini
else
  export HIPPIE_INI
fi

if [ ! -s "$HIPPIE_INI" ];then
  echo "Error: HIPPIE_INI not found ($HIPPIE_INI)"
  exit
else
 echo "USING INI File $HIPPIE_INI"
 source $HIPPIE_INI
fi

#Load Dataset config
if [ -e "$CONFIG" ];then
 if [ $(basename "$CONFIG") == "$CONFIG" ]
  then
   export HIPPIE_CFG="$PWD/$CONFIG"
  else
   export HIPPIE_CFG="$CONFIG"
 fi

else
 echo "Missing Config file, USAGE: hippie.sh [-i hippie.ini] [-h hippie_source] -f my_config.cfg"
 exit
fi

echo "Using CONFIG $HIPPIE_CFG"

source $HIPPIE_CFG
## make the stdout directory defined in the config file
mkdir -p ${STDOUT_DIR}
echo ${STDOUT_DIR}
## make the standard error directory
mkdir -p ${STDERR_DIR}
echo ${STDERR_DIR}

REFGENOME=$(basename $GENOME_REF .fasta)
FLOWCELLNAME=$(basename $FLOWCELLPATH)
echo "PROJECTNAME: $PROJECTNAME"
echo "FLOWCELLNAME: $FLOWCELLNAME"
echo "REFGENOME: $REFGENOME"
echo "DBSNP_VERSION: $DBSNP_VERSION"

###### generate launch script for each sample

count=1
for dir in  ${DATASET[@]};do
  echo $dir

  if [ -e "$dir" ];then
    mkdir -p "$dir/sam"
    mkdir -p "$dir/bed"
    mkdir -p "$dir/out"

    flist=($(ls $dir/fastq/*.fastq* 2>/dev/null));
    if [ ${#flist[@]} -eq 0 ]; then
	echo "WARNING: No fastq found in $dir/fastq"
    fi

    #for f in ${flist[@]}; do
    #		ln -s "$f" "$dir/fastq/" 2>/dev/null
    #	done;
    #

    # Copy templates to data directories, fill-in required values

    DATASET_NAME=$(basename $dir)
    DATASET_NAME=${sample[ $count ]}
    NEW_LAUNCHER="$dir/$DATASET_NAME.sh"

    cp "$CMD_DIR/hippie2_sample_template.sh" "$NEW_LAUNCHER"

    sed -i "s:HIPPIE_INI=:HIPPIE_INI=\"$HIPPIE_INI\":" "$NEW_LAUNCHER"
    sed -i "s:HIPPIE_CFG=:HIPPIE_CFG=\"$HIPPIE_CFG\":" "$NEW_LAUNCHER"

    # Fill-in Read Group (for bwa)
    sed -i \
      "s/RG_STR=/RG_STR=\'@RG\\\tID:${rgid[$count]}\\\tSM:${sample[$count]}\\\tPL:Illumina\\\tPU:${pu[$count]}\\\tLB:$LB\\\tDS:$DS\\\tCN:$CN\\\tDT:$DT\'/" \
      "$NEW_LAUNCHER"

    sed -i \
      "s/RG_STR_PICARD=/RG_STR_PICARD=\'ID=${rgid[$count]} SM=${sample[$count]} PL=Illumina PU=${pu[$count]} LB=$LB DS=$DS CN=$CN\'/" \
      "$NEW_LAUNCHER"

    sed -i "s/LINE=/LINE=$DATASET_NAME/" "$NEW_LAUNCHER"
    sed -i "s/RGID=/RGID=${rgid[$count]}/" "$NEW_LAUNCHER"
    sed -i "s/READLENGTH=/READLENGTH=${readlength[$count]}/" "$NEW_LAUNCHER"
    sed -i "s/CELL=/CELL=${cell[$count]}/" "$NEW_LAUNCHER"
    sed -i "s/RE=/RE=${re[$count]}/" "$NEW_LAUNCHER"
    sed -i "s/RESIZE=/RESIZE=${resize[$count]}/" "$NEW_LAUNCHER"
		if [ $USESEG == 1 ]; then
    sed -i "s/USESEG=0/USESEG=1/" "$NEW_LAUNCHER"
		fi
    sed -i "s/mbq=/mbq=$MBQ/" "$NEW_LAUNCHER"

    sed -i "s/mmq=/mmq=$MMQ/" "$NEW_LAUNCHER"

    sed -i "s:SAMPLE_DIR=:SAMPLE_DIR=$dir:" "$NEW_LAUNCHER"
    sed -i "s:FASTQ_DIR=:FASTQ_DIR=$dir/fastq:" "$NEW_LAUNCHER"
    sed -i "s:SAM_DIR=:SAM_DIR=$dir/sam:" "$NEW_LAUNCHER"
    sed -i "s:BED_DIR=:BED_DIR=$dir/bed:" "$NEW_LAUNCHER"
    sed -i "s:OUT_DIR=:OUT_DIR=$dir/out:" "$NEW_LAUNCHER"

  else
    echo "Error: Missing data directory $dir"
    exit
  fi

  # Perform command line run of phase based on "-pN" argument flag
  if [[ $DOPHASE1 ]]; then
    echo "Running Phase 1"
    echo "sh $NEW_LAUNCHER -p1"
    sh $NEW_LAUNCHER -p1
  fi

  if [[ $DOPHASE2 ]]; then
    echo "Running Phase 2"
    echo "sh $NEW_LAUNCHER -p2"
    sh $NEW_LAUNCHER -p2
  fi

  count=$[ $count + 1 ]
done # Loop Datasets
