# HiPPIE2
## an updated High-throughput Pipeline for identifying Physically Interacting Enhancer elements

### Method description:
Most regulatory chromatin interactions are mediated by various transcription factors (TFs) and
involve physically-interacting regions (PIRs) that are regulatory elements such as enhancers,
insulators, or promoters. To map these interactions across cell lines, we developed a novel
approach, HIPPIE2. HIPPIE2 accepts raw Hi-C reads as input and provides extensive
characterization of (1) PIRs and their potential regulatory roles using epigenomic annotations,
(2) high-confidence PIR-PIR interactions, and (3) mediating TF complexes based on binding
motifs and ChIP-seq experiments. Unlike standard genome binning approaches with fixed-sized
bins at 10K-1Mbp resolution, HIPPIE2 calls physical locations of PIRs with better precision and
higher resolution by using restriction enzyme cutting sites and strand orientations in the
ligation step of the Hi-C protocol.

### Instructions for use:
#### Obtaining source code:
To use HiPPIE2, pull the source code from [the bitbucket
repository](https://bitbucket.org/wanglab-upenn/HiPPIE2/).

#### Obtaining annotations and configuration templates:
We provide an archive containing all the relevant annotation files to run HiPPIE2 on the cell
lines described in our manuscript as well as configuration files that are easily updated to run
on your particular data path. The reference annotation files are [available for
download](https://tf.lisanwanglab.org/GADB/full_HIPPIE2_annotations.tar.gz), and can be
extracted and set up by running these steps. Note that for the config file updating script, you
need to give it the path to the Hi-C data you want to analyze and the name of the cell line as
the first and second arguments, respectively. This example shows what it would look like if you
had downloaded the GM12878 datasets to /path/to/GM12878_hic_data/:

```bash
## start from the directory containing full_HiPPIE2_annotations.tar.gz
$ tar -xzvf full_HiPPIE2_annotations.tar.gz
$ cd full_HiPPIE2_annotations/
$ ./update_config_file.sh /path/to/GM12878_hic_data/ "GM12878"
Configuration files are converted. Make sure to update the DATASET, sample, rgid, and cell arrays to correspond to your library in the GM12878_HiPPIE2_params.cfg file.
$ ls GM12878*
GM12878_HiPPIE2_params.cfg
GM12878_post_HiPPIE2_params.cfg
```

*Important note*: you still need to go into the first config file (GM12878_HiPPIE2_params.cfg,
 in this case) with any text editor and update the "DATASET", "sample", "rgid", and "cell"
 arrays to correspond to the cell line that you are running. These just tell HiPPIE2 how to
 look for the actual fastq data for mapping purposes, and so if you're running an experiment
 that wasn't part of the Rao et al manuscript, you can just update these to point to however
 your data is organized, as long as each dataset entry contains a fastq/ folder with split
 fastq files (see the scripts ending in _dump_and_split.sh for examples of how to generate
 this).

#### Data download and pre-processing
To re-run any of the Rao et al cell lines described in our manuscript, use the scripts in the data_preprocessing_scripts/ folder. For example, to download the NHEK data, run these scripts:

```bash
## this downloads the SRA file
$ cd /path/to/data_directory/
$ ./path/to/hi-c_motifs/data_preprocessing_scripts/NHEK_download_cmd.sh
## extract the fastq data and format for HiPPIE2 analysis
$ cd /path/to/hi-c_motifs/data_preprocessing_scripts/
$ ./NHEK_fastq_dump_and_split.sh /path/to/data_directory/
```

If the downloading script does not work due to broken paths, you can also directly use
fastq-dump with the relevant accession numbers; for example, if the file SRR1658693.sra doesn't
download correctly, you can set up a case statement to set SRA_FILES=`SRR1658693` for that Hi-C
library. This is the strategy used in the GM12878_rep1_fastq_dump_and_split.sh and
GM12878_rep2_fastq_dump_and_split.sh scripts so these can be edited for the relevant
library. Either way, this should generate fastq files split in the way that HiPPIE2 expects for
downstream processing.

#### Running Hi-C data through main HiPPIE2 processing pipeline
Once the Hi-C datasets are downloaded and in the split format required by HiPPIE2, a HiPPIE2
configuration file must be generated to run the pipeline (e.g. param_files/HIPPIE2/K562_hippie2.cfg). The required variables are as follows:

Config file variable | Value
-------------------- | -----
FLOWCELLPATH 	     | Main HiPPIE2 output directory
STDOUT_DIR 	     | Directory within the main output to save standard output
STDERR_DIR	     | Directory within the main output to save standard error	
GENOME_REF	     | The genome reference for mapping
GENOME_CHRM	     | The folder with chromosome fasta files for mapping
GENOME_PATH	     | The genomic reference file
RE_SITE		     | Bed file with the restriction enzyme binding sites for the RE used in this Hi-C experiment
SIZE_SELECT	     | Minimum size between interacting reads
ChimSegMin	     | Minimum size of a chimeric read
USESEG		     | Set to 0 (default) for enhancers specified by covering H3K4me1 or K27ac and DNase, but not H3K27me3 or H3K4me3; 1 for Enhancers specified by enhancers and weak enhancers from ENCODE Combined Segmentations tracks. 
MBQ		     | Minimum base quality
MMQ		     | Minimum mapping quality
PROJECTNAME	     | Sequencing project name (not used for anything)
BSUB_NODES	     | The name of the machines you want to use in bsub
DATASET		     | An array with the Hi-C library paths (e.g. DATASET[1]=${FLOWCELLPATH}/HIC065, DATASET[2]=${FLOWCELLPATH}/HIC066, ...)
sample		     | An array with the Hi-C library name (e.g. DATASET[1]=HIC065, DATASET[2]=HIC066, ...)
rgid		     | An array with the readgroup IDs (e.g. rgid[1]=hic065_MboI, rgid[2]=hic066_MboI, ...)
cell		     | An array containing the name of the cell line for every library
re		     | An array cotaining the name of the restriction enzyme for every library
resize		     | An array containing the size of the RE recognition motif for every library

Next, the HiPPIE2 initialization script (hi-c_motifs/HIPPIE2/hippie.ini) must be updated to point at the correct software and code paths:

Config file variable | Value
-------------------- | -----
HIPPIE_HOME 	     | HiPPIE2 code directory (within hi-c_motifs; e.g. /path/to/hi-c_motifs/HIPPIE2/)
CMD_HIPPIE	     | The cmd directory within the HiPPIE2 code. Shouldn't need to change this
CMD_DIR		     | Another variable that refers to the cmd directory, shouldn't need to change
TMPDIR		     | Temporary directory; by default, in the code directory, but could be anywhere
GENOMICS_BIN	     | The genomics bin directory. This doesn't get directly used, but needs to include STAR and Bedtools
STAR_HOME	     | The directory containing the STAR executable on your system. By default, it's relative to GENOMICS_BIN, but you could ignore GENOMICS_BIN and set this to wherever your STAR installation is
BEDTOOLS_HOME	     | Same as above but for bedtools2
BEDOPS_HOME	     | Same as above but for bedtops
RPATH		     | The path to your R installation directory
PYTHON		     | The path to the Python installation you are using
STAR		     | For readability in later scripts, this directly refers to the STAR executable

HiPPIE2 was developed using STAR2.4.0h, bedtools v2.19.1, and BedOPS v2.4.3. 

Once the initialization file is set up, the HiPPIE2 initialization script can be run as follows:

```bash
$ /path/to/hi-c_motifs/HIPPIE2/bin/hippie2.sh -f /path/to/HiPPIE2_cfg_file.cfg -i /path/to/hi-c_motifs/HIPPIE2/hippie2.ini -h /path/to/hi-c_motifs/HIPPIE2/
```

This creates a series of bash scripts in the Hi-C data folder, which can then be run individually as follows:

```bash
## go to the main HiPPIE2 output directory
$ cd ${FLOWCELLPATH}
## loop through the libraries and run the scripts
$ for DIR in HIC*; do echo ${DIR}; ${DIR}/${DIR}.sh -p1 > ${DIR}/${DIR}_HIPPIE2_cmd_log.txt; done
```

This will start running the main HiPPIE2 analysis including mapping and PIR identification. Once it's done running on your bsub cluster, you can run the functional annotation steps. 

#### Post-HiPPIE2 functional annotation
After running the main HiPPIE2 mapping pipeline on each library separately, the next step is to
run the 'post-HiPPIE2' steps which combine information across libraries and perform downstream
analysis including identifying physically interacting regions and annotating them with motif
and functional genomics datasets. This analysis is all run with the run_post_hippie2_steps.sh
script in the processing_scripts/ folder, which contains all the other scripts that run the
individual steps in this process. This script just takes one argument, the post-HiPPIE
configuration file, which requires the following variable definitions:

Config file variable | Value
-------------------- | -----
HIPPIE_FILES	     | A wild card expansion for all the folders containing the HiPPIE2 results on individual libraries
BASEDIR		     | The desired output directory for all the combined analysis results
MAPPABILITY_ANNOT_FOLDER   |	   The folder containing mappability bed files
NORM_PATH		   | The folder containing the Rao et al normalization matrices
RE			   | The name of the restriction enzyme used
RE_FILE			   | The bed file with restriction enzyme sites
SORT			   | An alias for parallel sort (from coreutils_8.6), or just normal sort if that's all you have installed
GEN_PATH		   | The folder with gene annotation information
BEDTOOLS		   | The path to your bedtools executable (same as in the first config file)
EPIGEN_PATH		   | The path to the folder containing epigenomic annotations for this cell type (more detail below)
CELL			   | The name of the cell line (used to refer to files in EPIGEN_PATH)
MOTIFPOS_PATH		   | The folder containing Factorbook data
HG19_G			   | A genome size file for hg19
BIOGRID_ALL		   | A file containing all the BioGRID interactions

For the epigenomic annotation pertaining to enhancer identification, the folder EPIGEN_PATH must contain the following files following a certain naming scheme, where ${CELL} refers to the cell type being analyzed and is defined in the post-HiPPIE2 configuration:

Epigenome file name | Contents
-------------------- | -----
${CELL}OpenChrom.gz  | Open chromatin annotations for this cell (DNase)
${CELL}CombSegEnh.gz | Enhancer annotations for this cell (typically by pulling out all the enhancer states from the Roadmap ChromHMM data)
${CELL}H3k27ac.gz    | H3K27ac ChIP-seq peaks for this cell
${CELL}H3k4me1.gz    | H3K4me1 ChIP-seq peaks for this cell
${CELL}H3k4me3.gz    | H3K4me3 ChIP-seq peaks for this cell
${CELL}H3k27me3.gz   | H3K27me3 ChIP-seq peaks for this cell

Then, the post-HiPPIE2 steps can be run directly as follows, or wrapped in a bsub call:

```bash
$ /path/to/hi-c_motifs/processing_scripts/
$ time ./run_post_hippie2_steps.sh /path/to/post_hippie_CELL_analysis.cfg
```

This script will take quite some time to run some of the steps, so this should be wrapped in a
screen session on an interactive server, or submitted as a job to your cluster system of
choice.
