#!/bin/sh

## GM12878_fastq_dump_and_split.sh
## alex amlie-wolf 
## recording the command to get fastq data from SRA files for GM12878
module load sratoolkit/2.8.0

GM12878_DIR=$1 
mkdir -p ${GM12878_DIR}
cd ${GM12878_DIR}

for DIR in HIC0{03..18}; do
    mkdir -p ${DIR}/fastq/
    case "$DIR" in
	HIC003) SRA_FILES=`SRR1658572`
	    ;;
	HIC004) SRA_FILES=`SRR1658573`
	    ;;
	HIC005) SRA_FILES=`SRR1658574 SRR1658575 SRR1658576`
	    ;;
	HIC006) SRA_FILES=`SRR1658577`
	    ;;
	HIC007) SRA_FILES=`SRR1658578`
	    ;;
	HIC008) SRA_FILES=`SRR1658579`
	    ;;
	HIC009) SRA_FILES=`SRR1658580`
	    ;;
	HIC010) SRA_FILES=`SRR1658581`
	    ;;
	HIC011) SRA_FILES=`SRR1658582`
	    ;;
	HIC012) SRA_FILES=`SRR1658583`
	    ;;
	HIC013) SRA_FILES=`SRR1658584`
	    ;;
	HIC014) SRA_FILES=`SRR1658585 SRR1658586`
	    ;;
	HIC015) SRA_FILES=`SRR1658587`
	    ;;
	HIC016) SRA_FILES=`SRR1658588`
	    ;;
	HIC017) SRA_FILES=`SRR1658589`
	    ;;
	HIC018) SRA_FILES=`SRR1658590`
	    ;;
	esac

    for SRA_FILE in $SRA_FILES; do
	echo "Dumping ${SRA_FILE}"
	## the -I flag breaks Yih-Chii's downstream processing
	time fastq-dump ${SRA_FILE} -O ${DIR}/fastq/ --split-files 
	for FQ in ${DIR}/fastq/*.fastq; do
	    echo "Splitting ${FQ}"
	    time split -l 80000000 ${FQ} ${FQ%%.*}_ -a4 -d
	    echo "Compressing results"
	    for i in ${FQ%%.*}_*; do
		gzip -c ${i} > ${i}.fastq.gz
		## remove the un-compressed file
		rm ${i}
	    done
	    ## remove the file to save space
	    rm ${FQ}
	done
    done
done

cd -
