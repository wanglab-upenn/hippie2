cd ~/data/hi-c_motifs/rao_dataHUVEC

wget -P HIC080 ftp://ftp-trace.ncbi.nlm.nih.gov/sra/sra-instant/reads/ByExp/sra/SRX/SRX765/SRX765015/SRR1658709/SRR1658709.sra; \
    wget -P HIC080 ftp://ftp-trace.ncbi.nlm.nih.gov/sra/sra-instant/reads/ByExp/sra/SRX/SRX765/SRX765015/SRR1658710/SRR1658710.sra; \
    wget -P HIC080 ftp://ftp-trace.ncbi.nlm.nih.gov/sra/sra-instant/reads/ByExp/sra/SRX/SRX765/SRX765015/SRR1658711/SRR1658711.sra; \
    wget -P HIC081 ftp://ftp-trace.ncbi.nlm.nih.gov/sra/sra-instant/reads/ByExp/sra/SRX/SRX765/SRX765016/SRR1658712/SRR1658712.sra; \
    wget -P HIC082 ftp://ftp-trace.ncbi.nlm.nih.gov/sra/sra-instant/reads/ByExp/sra/SRX/SRX765/SRX765017/SRR1658713/SRR1658713.sra; \
    wget -P HIC082 ftp://ftp-trace.ncbi.nlm.nih.gov/sra/sra-instant/reads/ByExp/sra/SRX/SRX765/SRX765017/SRR1658714/SRR1658714.sra
