#!/bin/sh

# set -x

## subsample_fastq_files.sh
## alex amlie-wolf 01/15/19
## a script to subsample fastq files from a hi-c experiment, following HIPPIE2 data organization

## first define the directory that this will all be done in
OUTDIR=$1
shift
SEQTK=$1
shift

mkdir -p ${OUTDIR}

for CFG_FILE in $*; do
    echo $CFG_FILE
    source ${CFG_FILE}

    THIS_CELL=`basename ${FLOWCELLPATH}`

    ## make a new directory for this cell type
    mkdir -p ${OUTDIR}/${THIS_CELL}/

    ## now iterate through amount of subsampling
    for SAMP_FRAC in 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0; do
	date
	echo "Sampling ${SAMP_FRAC} of reads"
	for HIC_LIB in ${FLOWCELLPATH}/HIC*/; do
	    THIS_LIB=`basename ${HIC_LIB}`
	    echo $THIS_LIB
	    THIS_OUT=${OUTDIR}/${THIS_CELL}/subsampling_${SAMP_FRAC}/${THIS_LIB}/fastq/
	    mkdir -p ${THIS_OUT}
	    
	    ## now subsample the original fastq files and move them over
	    ## this is tricky because they are paired, need to retain relationships
	    for FQ in `ls ${HIC_LIB}/fastq/*.fastq.gz | sed -n 's/.*\(SRR[0-9]\+_1_[0-9]\+.*\)/\1/p'`; do
		## need to also get the secondary fastq file
		SEC_FQ=`echo $FQ | sed 's/_1_/_2_/'`

		## use the same seed to get the same samples
		THIS_SEED=$((1 + RANDOM % 1000000))
		echo "Seed: ${THIS_SEED} ($FQ, $SEC_FQ)"

		## get the exact location of the file
		FQ_PATH=`readlink -m ${HIC_LIB}/fastq/${FQ}`
		${SEQTK} sample -s${THIS_SEED} ${FQ_PATH} ${SAMP_FRAC} | \
		    gzip > ${THIS_OUT}/${FQ}

		SEC_FQ_PATH=`readlink -m ${HIC_LIB}/fastq/${SEC_FQ}`
		${SEQTK} sample -s${THIS_SEED} ${SEC_FQ_PATH} ${SAMP_FRAC} | \
		    gzip > ${THIS_OUT}/${SEC_FQ}
	    done
	done
    done
done
