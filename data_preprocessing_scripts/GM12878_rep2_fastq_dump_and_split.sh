#!/bin/sh

## GM12878_rep2_fastq_dump_and_split.sh
## alex amlie-wolf 
## recording the command to get fastq data from SRA files for the second GM12878 replicate
module load sratoolkit/2.8.0

GM12878_DIR=$1 
mkdir -p ${GM12878_DIR}
cd ${GM12878_DIR}

for DIR in HIC0{19..29}; do
    mkdir -p ${DIR}/fastq/
    case "$DIR" in
	HIC019) SRA_FILES=`SRR1658591`
	    ;;
	HIC020) SRA_FILES=`SRR1658592`
	    ;;
	HIC021) SRA_FILES=`SRR1658593`
	    ;;
	HIC022) SRA_FILES=`SRR1658594 SRR1658595`
	    ;;
	HIC023) SRA_FILES=`SRR1658596 SRR1658597`
	    ;;
	HIC024) SRA_FILES=`SRR1658598`
	    ;;
	HIC025) SRA_FILES=`SRR1658599`
	    ;;
	HIC026) SRA_FILES=`SRR1658600`
	    ;;
	HIC027) SRA_FILES=`SRR1658601`
	    ;;
	HIC028) SRA_FILES=`SRR1658602`
	    ;;
	HIC029) SRA_FILES=`SRR1658603`
	    ;;
	esac

    for SRA_FILE in $SRA_FILES; do
	echo "Dumping ${SRA_FILE}"
	## the -I flag breaks Yih-Chii's downstream processing
	time fastq-dump ${SRA_FILE} -O ${DIR}/fastq/ --split-files 
	for FQ in ${DIR}/fastq/*.fastq; do
	    echo "Splitting ${FQ}"
	    time split -l 80000000 ${FQ} ${FQ%%.*}_ -a4 -d
	    echo "Compressing results"
	    for i in ${FQ%%.*}_*; do
		gzip -c ${i} > ${i}.fastq.gz
		## remove the un-compressed file
		rm ${i}
	    done
	    ## remove the file to save space
	    rm ${FQ}
	done
    done
done

cd -
