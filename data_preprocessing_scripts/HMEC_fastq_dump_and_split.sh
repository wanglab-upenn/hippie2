#!/bin/sh

## HMEC_fastq_dump.sh
## alex amlie-wolf 06/16/17
## recording the command to get fastq data from SRA files for HMEC
module load sratoolkit/2.8.0

HMEC_DIR=$1 # /home/alexaml/data/hi-c_motifs/rao_data/HMEC/
cd ${HMEC_DIR}

for DIR in HIC058 HIC059 HIC060 HIC061 HIC062 HIC063; do
    mkdir -p ${DIR}/fastq/
    SRA_FILE=`ls ${DIR}/*.sra`
    echo "Dumping ${SRA_FILE}"
    ## the -I flag breaks Yih-Chii's downstream processing
    time fastq-dump ${SRA_FILE} -O ${DIR}/fastq/ --split-files 
    for FQ in ${DIR}/fastq/*.fastq; do
	echo "Splitting ${FQ}"
	time split -l 80000000 ${FQ} ${FQ%%.*}_ -a4 -d
	echo "Compressing results"
	for i in ${FQ%%.*}_*; do
	    gzip -c ${i} > ${i}.fastq.gz
	    ## remove the un-compressed file
	    rm ${i}
	done
	## remove the file to save space
	rm ${FQ}
    done
done

cd -
