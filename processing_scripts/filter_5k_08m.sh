#!/bin/sh

## filter_5k_08m.sh
## alex amlie-wolf, adapted from yih-chii hwang's script
## filter out restriction fragments that are longer than 5 kb or don't have good mappability

if [ $# == 1 ]; then
    ## this takes in the base directory
    BASEDIR=$1
    INDIR=${BASEDIR}/aggregate_interRS/
    OUTDIR=${BASEDIR}/filter_5k_08m/
    mkdir -p ${OUTDIR}

    for i in {1..22} X Y; do
	echo "Filtering chr$i";
	awk '{if ($10>5000 && $12 >= 0.8 && $14 >=0.8)print }' ${INDIR}/chr${i}_interRS_merged.txt \
	    > ${OUTDIR}/chr${i}_interRS_d5kM08filtered.txt
    done

    echo "Filtering inter-chromosomal interactions"
    awk '{if ($12 >= 0.8 && $14 >=0.8)print }' ${INDIR}/interChrm_interRS_merged.txt \
	> ${OUTDIR}/interChrm_interRS_d5kM08filtered.txt
else
    echo "Usage: $0 <data directory>"
fi
