## this was in motif_occurrence.sh

for i in {1..1000}; do
    $BEDTOOLS intersect -b shuffle_peak/hic_normPeak_${i}.bed -a ${MOTIFPOS_PATH}/factorbookMotifPos.bed.gz -sorted -f 1.0 -wb|awk 'BEGIN{OFS="\t"}{print $7,$8,$9,$4,$5,$6}'|$BEDTOOLS merge -i - -c 4,4 -o collapse,count > shuffle_peak/shuffle_peak_containedMotif_${i}.bed
    done

    # get motifs contained in non-hic-peaks
    # calculate enrichment level of each motifs
    # EI = (# motifs in hic peaks/sum length of hicpeaks)/(# motifs in non-peak/total length non-peak)
    motif_array=($(zcat  ${MOTIFPOS_PATH}/factorbookMotifPos.bed.gz|cut -f4|sort -u))
    export HIC_PEAK_LEN=`awk 'BEGIN{sum=0}{sum+=$3-$2}END{print sum}' hic_normPeak_l4k_sorted.bed`
    export HIC_NONPEAK_LEN=`awk 'BEGIN{sum=0}{sum+=$3-$2}END{print sum}' shuffle_peak/hic_normPeak_1.bed`
    echo -n "" > hic_normPeak_motif_occurance.txt
    for MOTIF_ID in "${motif_array[@]}";do
    # nominator
    	    string=""
    	    export HIC_MOTIFS=`awk -v motif_id=${MOTIF_ID} 'BEGIN{sum=0}{split($4, a, ",");for (i in a) {if (a[i]==motif_id) sum+=1}}END{print sum}' hic_normPeak_containedMotif.bed`
    	    string=`echo -e $string"\t"$HIC_MOTIFS`
    # denominator
    	    echo $MOTIF_ID
    	    for i in {1..999}; do 
    		    export BG_MOTIFS=`awk -v motif_id=${MOTIF_ID} 'BEGIN{sum=0}{split($4, a, ",");for (i in a) {if (a[i]==motif_id) sum+=1}}END{print sum}' shuffle_peak/shuffle_peak_containedMotif_${i}.bed`
    		    string=`echo -e $string"\t"$BG_MOTIFS`
    	    done

    	    i=1000
    	    export BG_MOTIFS=`awk -v motif_id=${MOTIF_ID} 'BEGIN{sum=0}{split($4, a, ",");for (i in a) {if (a[i]==motif_id) sum+=1}}END{print sum}' shuffle_peak/shuffle_peak_containedMotif_${i}.bed`
    	    echo $BG_MOTIFS
    	    string=`echo -e $string"\t"$BG_MOTIFS`
    	    echo -e $MOTIF_ID"\t"$string >> hic_normPeak_motif_occurance.txt
    done;
