#!/bin/sh

## motif_interaction.sh
## alex amlie-wolf adapted from yih-chii hwang
## intersects PIR-PIR pairs and motif annotations

# export MOTIFPOS_PATH="/home/yihhwang/analysis/encode_tfbs"

if [ $# == 2 ]; then
    BASEDIR=$1
    MOTIFPOS_PATH=$2
    
    OUTDIR=${BASEDIR}/motif_interaction/
    mkdir -p ${OUTDIR}
    
    # extract motif interactions
    echo "Extracting motif interactions"
    awk 'BEGIN{OFS="\t";FS="\t"} 
		    NR==FNR{result=""; delete uMotif; split($8,motifs,",");
                            for (i in motifs){uMotif[motifs[i]]=motifs[i];}
                            for (j in uMotif){result=result","uMotif[j]} a[$1,$2]=substr(result,2);next;}
		    {if (($1,$2) in a){left=$1"\t"$2"\t"$3"\t"$4"\t"a[$1,$2]; }
                         else{left=$1"\t"$2"\t"$3"\t"$4"\tNA";} 
	             if (($5,$6) in a){right=$5"\t"$6"\t"$7"\t"$8"\t"a[$5,$6]; }
                            else{right=$5"\t"$6"\t"$7"\t"$8"\tNA";} 
			    print left,right; }' \
        ${BASEDIR}/annotate_interactor_motif/all_norm_interactor_anno_motif.txt \
	${BASEDIR}/annotateNorm_interactor/all_norm_interaction_anno2nd.txt \
				> ${OUTDIR}/all_norm_interaction_anno_motif.txt

    mkdir -p ${OUTDIR}/motif_contingency_normTable

    ## counting motifs across annotations
    echo "Counting motifs across annotations"
    awk 'BEGIN{OFS="\t";FS="\t"}{table[$4"\t"$9]+=1;if($4!=$9){table[$9"\t"$4]+=1;}
                                 }END{for (x in table) print x,table[x]}
    ' ${OUTDIR}/all_norm_interaction_anno_motif.txt > \
	${OUTDIR}/motif_contingency_normTable/motif_contingency_normTable.txt

    ## commented this out because it never seems to get used..
#    mkdir -p ${OUTDIR}/motif_prob_normTable

    ## listing motifs
    echo "Counting motif occurences"
    awk 'BEGIN{OFS="\t";FS="\t"}{split("",a,":");split ($5,leftM,","); split ($10,rightM,","); 
	    for (i in leftM) {if (a[leftM[i]]!=1) {count[leftM[i]]+=1; a[leftM[i]]=1;}}
	    for (j in rightM) {if (a[rightM[j]]!=1) {count[rightM[j]]+=1; a[rightM[j]]=1;}}
                                 }END{for (x in count) print x,count[x]}
    ' ${OUTDIR}/all_norm_interaction_anno_motif.txt \
	> ${OUTDIR}/motif_existence_interaction_list.txt

    echo "Counting individual motif occurrences"
    motif_array=($(zcat ${MOTIFPOS_PATH}/factorbookMotifPos.bed.gz|cut -f4|sort|uniq))

    for motif in ${motif_array[@]}; do 
	    awk -v motif=${motif} 'BEGIN{OFS="\t";FS="\t"}{split ($5,leftM,","); split ($10,rightM,","); 
	    for (i in leftM) {if (leftM[i]==motif) {table[$4"\t"$9]+=1;}}
	    for (j in rightM ) {if (rightM[j]==motif && $4!=$9) {table[$9"\t"$4]+=1;}}
	    }END{for (x in table) print x,table[x]}
	    ' ${OUTDIR}/all_norm_interaction_anno_motif.txt \
		> ${OUTDIR}/motif_contingency_normTable/motif_contingency_normTable_"${motif}".txt
    done
else
    echo "Usage: $0 <data directory> <motif position path>"
fi
