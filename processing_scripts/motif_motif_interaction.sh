#!/bin/sh

## motif_motif_interaction.sh
## alex amlie-wolf adapted from yih-chii hwang
## takes the output of motif_motif_interaction.txt and counts the number of enhancer-promoter
## pairs supporting each motif interaction

if [ $# == 1 ]; then
    BASEDIR=$1
    INDIR=${BASEDIR}/motif_interaction/
    OUTDIR=${BASEDIR}/motif_motif_interaction/
    mkdir -p ${OUTDIR}
    
    # get motif-motif interaction in enhancer-promoter pairs
    awk 'BEGIN{OFS="\t";FS="\t"}
	    {split("",eM,":");split("",pM,":");split("",e,":");split("",p,":");
     if($4=="enhancer"&&$9=="promoter"){
	    split($5,eM,",");split($10,pM,",");
	    for (i in eM){e[eM[i]]=1};for (j in pM){p[pM[j]]=1}
	    for (i in e){for (j in p){print i,j}}}
     else if ($4=="promoter"&&$9=="enhancer"){
      split($10,eM,",");split($5,pM,",");
      for (i in eM){e[eM[i]]=1};for (j in pM){p[pM[j]]=1}
      for (i in e){for (j in p){print i,j}}}
    }' ${INDIR}/all_norm_interaction_anno_motif.txt \
	> ${OUTDIR}/motif_motif_ep_interaction.txt

    awk '{if($1!="NA" && $2!="NA" && $1!~/^UA/&& $2!~/^UA/)print}' \
	${OUTDIR}/motif_motif_ep_interaction.txt | sort | uniq -c | sort -k1,1 -k2,2 | \
	awk 'BEGIN{OFS="\t"}{print $2,$3,$1}' > ${OUTDIR}/motif_motif_ep_count.txt
else
    echo "Usage: $0 <data directory>"
fi

