#!/bin/sh

## hicPeakNorm.sh
## alex amlie-wolf adapted from yih-chii hwang
## determine the PIRs

## for now, just leave these variables in
# export RE="MboI"

if [ $# == 4 ]; then
    BASEDIR=$1
    INDIR=${BASEDIR}/normalization/
    OUTDIR=${BASEDIR}/hicPeakNorm/
    mkdir -p ${OUTDIR}
    
    ## this is the name of the restriction enzyme
    RE=$2
    ## this is the file corresponding to the R.E. sites
    RE_FILE=$3 
    SORT=$4
    
    for i in {1..22} X; do
	echo "generating restriciton site read distribution for chr${i}"
	
	# collapse reads toward each restriction site
	awk 'BEGIN{OFS="\t"}{print $1,$2,$3,$4,$9,$18,$15;print $5,$6,$7,$8,$9,$18,$15}' \
	    ${INDIR}/chr${i}_interRS_normalized.txt | $SORT -k2,2n > \
	    ${OUTDIR}/chr${i}_${RE}_normRead.bed
	
	awk 'BEGIN{first=1;OFS="\t";count=0;presite=""}
		{if (presite!=$1"\t"$2"\t"$3"\t"$4 && first!=1){ 
			print presite,count,read;count=0;read="";} 
			if (count==0){read=$7;}else{read=read";"$7} 
			presite=$1"\t"$2"\t"$3"\t"$4; count+=$6;first=0}
		END{print presite,count,read}' \
			    ${OUTDIR}/chr${i}_${RE}_normRead.bed > \
			    ${OUTDIR}/chr${i}_${RE}_normRead_merged.bed

	export med=`cut -f 5 ${OUTDIR}/chr${i}_${RE}_normRead_merged.bed | sort -k1,1n | awk '{a[NR]=$1}END{print a[NR/2]}'`
	echo "The median read depth of chr${i}: med=${med}"
	export dist_thre=`awk -v chr=chr${i} 'BEGIN{preEnd=0}{if($1==chr) {print $2-preEnd;preEnd=$3}}' ${RE_FILE} | sort -k1,1n | awk '{a[NR]=$1}END{print a[int(NR/(4/3))]}'`
	echo "The 75% restriction site distance of chr${i}, ${dist_thre}"

	echo "Identifying boundary starts for chr${i}"
	awk -v median=$med -v d_thre=$dist_thre \
	    'BEGIN{preSite=0;sum=0;max=0;maxHit="";OFS="\t";first=1;read=""} 
                  { if($4=="d"){ if(first==1){read=$6} else{read=read";"$6}
			         sum+=$5;
			         if ($5>=median) {
                                    if( $2-preSite > d_thre && first == 0 ) {
                                       print maxHit,max,maxSum,maxRead;max=0;sum=$5;read=$6} 
                               	    if ($5 >= max) {
                                       maxHit=$1"\t"$2"\t"$3; max = $5;maxSum=sum;maxRead=read;}
                         	    preSite = $3; first=0; }}}' \
	    ${OUTDIR}/chr${i}_${RE}_normRead_merged.bed > ${OUTDIR}/chr${i}_normStart_boundary.bed

	echo "Identifying boundary ends for chr${i}"
	tac ${OUTDIR}/chr${i}_${RE}_normRead_merged.bed | \
	    awk -v median=$med -v d_thre=$dist_thre \
	    'BEGIN{preSite=0;sum=0;max=0;maxHit="";OFS="\t";first=1;read=""} 
                  { if($4 == "u"){ if (first==1){read=$6} else{read=read";"$6}
                     		   sum+=$5; 
			           if ($5>=median) {
     	                              if( preSite-$2 > d_thre && first==0 ) {
                                         print maxHit,max,maxSum,maxRead;max=0;sum=$5;read=$6;} 
       	                              if ($5 >= max) {
                                         maxHit=$1"\t"$2"\t"$3; max = $5;maxSum=sum;maxRead=read;}
       	                              preSite = $3; first=0; }}}' | \
	    tac > ${OUTDIR}/chr${i}_normEnd_boundary.bed

	echo "Pairing boundaries for chr${i}"
	python2.7 ./pair_boundary_norm.py chr${i} ${OUTDIR}/chr${i}_normStart_boundary.bed \
	    ${OUTDIR}/chr${i}_normEnd_boundary.bed ${OUTDIR}/chr${i}_hic_normPeak.bed \
	    ${OUTDIR}/chr${i}_norm_unmatch_boundary.bed

    done
else
    echo "Usage: $0 <data directory> <restriction enzyme name> <restriction enzyme file> <sort command>"
fi


# check overlap with CTCF and open chromatin sit
# 1. overlap event count
#bedtools intersect -a openChrom_chr1.bed -b chr1_hic_peak.bed -wa -u|wc -l
# 2. overlap coverage
#cut -f 1-3 openChrom_chr1.bed| bedtools annotate -i - -files chr1_hic_peak.bed|awk '{if($4>0){sum+=$4;count+=1}}END{print sum/count}' 
# 1.
#bedtools intersect -a chr1_spp_CTCF.bed -b chr1_hic_peak.bed -wa -u|wc -l 
# 2. 
#cut -f 1-3 chr1_spp_CTCF.bed| bedtools annotate -i - -files chr1_hic_peak.bed|awk '{if($4>0){sum+=$4;count+=1}}END{print sum/count}'

