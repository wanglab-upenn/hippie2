#!/bin/sh

## make_UCSC_interact_file.sh
## alex amlie-wolf 01/28/19
## a script to convert the hic_normOut_merged_sig.txt peak files to UCSC interaction files

## default option, for GM12878 primary
if [ $# == 2 ] ; then
    INFILE=$1
    OUTFILE=$2    
    
    ## name is peak_i_left or peak_i_right
    ## score is negative log10(q-value), rounded to nearest integer
    ## value is number of normalized reads ($8)
    ## color is 0, shaded by score (needs spectrum setting)
    
    ## first set up the header
    echo -e 'track type=interact name="Significant GM12878 HIPPIE2 PIR-PIR Interactions" description="GM12878 rep1 PIR-PIR interactions" useScore=on maxHeightPixels=200 visibility=full' > ${OUTFILE}     
    awk -F$'\t' 'BEGIN{OFS=FS; COUNT=1} {LOG_QVAL=-log($17)/log(10); if(LOG_QVAL=="inf") {LOG_QVAL=0}; print $1, $2, $6, "PIR_PIR_"COUNT, int(LOG_QVAL), $8, ".", 0, $1, $2, $3, "PIR_PIR_"COUNT"_left", ".", $4, $5, $6, "PIR_PIR_"COUNT"_right", "."; COUNT+=1}' \
	${INFILE} >> ${OUTFILE}
elif [ $# == 3 ] ; then
    INFILE=$1
    OUTFILE=$2
    CELL=$3
    
    ## name is peak_i_left or peak_i_right
    ## score is negative log10(q-value), rounded to nearest integer
    ## value is number of normalized reads ($8)
    ## color is 0, shaded by score (needs spectrum setting)
    
    ## first set up the header
    echo -e 'track type=interact name="Significant '"${CELL}"' HIPPIE2 PIR-PIR Interactions" description="'"${CELL}"' PIR-PIR interactions" useScore=on maxHeightPixels=200 visibility=full' > ${OUTFILE}     
    awk -F$'\t' 'BEGIN{OFS=FS; COUNT=1} {LOG_QVAL=-log($17)/log(10); if(LOG_QVAL=="inf") {LOG_QVAL=0}; print $1, $2, $6, "PIR_PIR_"COUNT, int(LOG_QVAL), $8, ".", 0, $1, $2, $3, "PIR_PIR_"COUNT"_left", ".", $4, $5, $6, "PIR_PIR_"COUNT"_right", "."; COUNT+=1}' \
	${INFILE} >> ${OUTFILE}
fi


