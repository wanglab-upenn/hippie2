import sys, re, time

peakFile = sys.argv[1]
outFile = sys.argv[2]

def peak_reader(peakFile, outFile):
	fOut = open(outFile, 'w')
	readIdDict = dict()
	for l in open(peakFile):
		v = l.rstrip().split()
		readIdList = v[6].split(";")
		for i in xrange (1,len(readIdList)):
			if readIdList[i] not in readIdDict:
				readIdDict[readIdList[i]] = [v[0],v[1],v[2]]
			else:
				fOut.write ("\t".join([str(s) for s in [readIdDict[readIdList[i]][0],readIdDict[readIdList[i]][1],readIdDict[readIdList[i]][2],v[0],v[1],v[2],readIdList[i]]])+"\n")
				del readIdDict[readIdList[i]]
	fOut.close()
	

sys.stdout.write( "["+time.strftime("%c")+"] Start finding peak interaction... "+"\n")
sys.stdout.flush()

sys.stdout.write( "["+time.strftime("%c")+"] Reading the peak file... "+"\n")
sys.stdout.flush()
start = time.clock()
peak_reader(peakFile, outFile)
sys.stdout.write( "Time reading peak file: "+ str(time.clock()-start)+ " s"+"\n")
sys.stdout.flush()



