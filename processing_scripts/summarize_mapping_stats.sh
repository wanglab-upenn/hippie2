#!/bin/sh

## summarize_mapping_stats.sh
if [ $# -ge 2 ]; then
    ## save the directory for the combined analysis output folder
    BASEDIR=$1
    shift
    CELL=$1
    shift
    OUTDIR=${BASEDIR}/full_HIPPIE2_summary/
    mkdir -p ${OUTDIR}

    ## set up the header for the read stats
    echo -e "HIC_sample\tSRR_ID\tREAD_TYPE\tREAD_NUM" > ${OUTDIR}/${CELL}_mapping_stats.txt
    
    ## start by summarizing mapping results from individual sides of paired reads
    echo "Summarizing mapping results"
    for DIR in $*; do
    	HIC_SAMPLE=`basename $DIR`
    	for LOGF in ${DIR}/sam/*Log.final.out; do
    	    SRR_ID=`basename $LOGF | sed -e 's/Log.final.out//g'`
    	    ## count various types of reads:
    	    ## input reads
    	    INREADS=`grep "Number of input reads" $LOGF | cut -f2`
    	    echo -e "${HIC_SAMPLE}\t${SRR_ID}\tinput_reads\t${INREADS}" >> ${OUTDIR}/${CELL}_mapping_stats.txt
    	    ## uniquely mapped read number
    	    UNIQREADS=`grep "Uniquely mapped reads number" $LOGF | cut -f2`
    	    echo -e "${HIC_SAMPLE}\t${SRR_ID}\tuniq_map_reads\t${UNIQREADS}" >> ${OUTDIR}/${CELL}_mapping_stats.txt
    	    ## average input length
    	    AVG_IN_LENGTH=`grep "Average input read length" $LOGF | cut -f2`
    	    echo -e "${HIC_SAMPLE}\t${SRR_ID}\tavg_input_length\t${AVG_IN_LENGTH}" >> ${OUTDIR}/${CELL}_mapping_stats.txt	    	    
    	    ## average mapped length
    	    AVG_MAP_LENGTH=`grep "Average mapped length" $LOGF | cut -f2`
    	    echo -e "${HIC_SAMPLE}\t${SRR_ID}\tavg_map_length\t${AVG_MAP_LENGTH}" >> ${OUTDIR}/${CELL}_mapping_stats.txt	    
    	    ## multiple loci mapped (should always be 0)
    	    MULTI_MAPPED=`grep "Number of reads mapped to multiple loci" $LOGF | cut -f2`
    	    echo -e "${HIC_SAMPLE}\t${SRR_ID}\tmultimap_reads\t${MULTI_MAPPED}" >> ${OUTDIR}/${CELL}_mapping_stats.txt	    	    
    	    ## number of reads mapped to too many loci
    	    NUM_TOO_MANY=`grep "Number of reads mapped to too many loci" $LOGF | cut -f2`
    	    echo -e "${HIC_SAMPLE}\t${SRR_ID}\ttoo_many_loci\t${NUM_TOO_MANY}" >> ${OUTDIR}/${CELL}_mapping_stats.txt	    	    
    	    ## too many mismatch percentage
    	    MISMATCH_PCT=`grep "unmapped: too many mismatches" $LOGF | cut -f2 `
    	    echo -e "${HIC_SAMPLE}\t${SRR_ID}\tmismatch_pct\t${MISMATCH_PCT%\%}" >> ${OUTDIR}/${CELL}_mapping_stats.txt	    	    	    
    	    ## too short read percentage
    	    TOOSHORT_PCT=`grep "unmapped: too short" $LOGF | cut -f2`
    	    echo -e "${HIC_SAMPLE}\t${SRR_ID}\ttoo_short_pct\t${TOOSHORT_PCT%\%}" >> ${OUTDIR}/${CELL}_mapping_stats.txt	    	    	    	    
    	    ## unmapped 'other' percentage
    	    OTHER_PCT=`grep "unmapped: other" $LOGF | cut -f2`
    	    echo -e "${HIC_SAMPLE}\t${SRR_ID}\tunmapped_other_pct\t${OTHER_PCT%\%}" >> ${OUTDIR}/${CELL}_mapping_stats.txt
    	done
	## now count all the RS assignments and direction pairs
	echo "Characterizing read pairs for ${HIC_SAMPLE}"
	PAIR_STATS=`cut -f17 ${DIR}/out/*_direction.txt | sort | uniq -c`
	## now loop through these pairs and do the right assignments
	while read -r NUM COND; do	    
	    case "$COND" in
		"ambiguousBothLegit" ) AMBIGUOUS_NORMAL=$NUM;;
		"ambiguousSameDist" ) AMBIGUOUS_CHIMERIC=$NUM;;
		"bad" ) BAD_CHIMERIC=$NUM;;
		"good" ) GOOD_NORMAL=$NUM;;
		"goodC" ) GOOD_CHIMERIC=$NUM;;
		"noneSpecific" ) BAD_NORMAL=$NUM;;
	    esac
	done <<< "$(printf "%s %s\n" $PAIR_STATS)"

	## also get sum of all read pairs
	RS_SUM=`echo | awk -v NORMAL=${GOOD_NORMAL} -v CHIMERIC=${GOOD_CHIMERIC} '{print NORMAL+CHIMERIC}'`
	echo -e "${HIC_SAMPLE}\tsummary\tnon_specific_normal\t${BAD_NORMAL}" >> ${OUTDIR}/${CELL}_mapping_stats.txt
	echo -e "${HIC_SAMPLE}\tsummary\tnon_specific_chimeric\t${BAD_CHIMERIC}" >> ${OUTDIR}/${CELL}_mapping_stats.txt
	echo -e "${HIC_SAMPLE}\tsummary\tambiguous_direction_normal\t${AMBIGUOUS_NORMAL}" >> ${OUTDIR}/${CELL}_mapping_stats.txt
	echo -e "${HIC_SAMPLE}\tsummary\tambiguous_direction_chimeric\t${AMBIGUOUS_CHIMERIC}" >> ${OUTDIR}/${CELL}_mapping_stats.txt		
	echo -e "${HIC_SAMPLE}\tsummary\tnormal_RS_pairs\t${GOOD_NORMAL}" >> ${OUTDIR}/${CELL}_mapping_stats.txt
	echo -e "${HIC_SAMPLE}\tsummary\tchimeric_RS_pairs\t${GOOD_CHIMERIC}" >> ${OUTDIR}/${CELL}_mapping_stats.txt
	echo -e "${HIC_SAMPLE}\tsummary\tall_RS_pairs\t${RS_SUM}" >> ${OUTDIR}/${CELL}_mapping_stats.txt	
    done
    
else
    echo "Usage: $0 <combined analysis outdir> <HIPPIE2 folder 1> <HIPPIE2 folder 2> ..."
fi
