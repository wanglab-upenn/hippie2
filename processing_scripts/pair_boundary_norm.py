import sys, re, time

chrm=sys.argv[1]
leftBoundFile = sys.argv[2]
rightBoundFile = sys.argv[3]
outFile = sys.argv[4]
outFile_unmatchleft = sys.argv[5]

def left_bound_reader(leftBoundFile):
	leftReStart = dict()
	leftReEnd = dict()
	leftReRead = dict()
	leftReReadSum = dict()
	leftReId = dict()
	for l in open(leftBoundFile):
		v = l.rstrip().split()
		if v[0] not in leftReStart:
			leftReStart[v[0]] = list()
			leftReEnd[v[0]] = list()
			leftReRead[v[0]] = list()
			leftReReadSum[v[0]] = list()
			leftReId[v[0]] = list()
		leftReStart[v[0]].append(int(v[1]))
		leftReEnd[v[0]].append(int(v[2]))
		leftReRead[v[0]].append(float(v[3]))
		leftReReadSum[v[0]].append(float(v[4]))
		leftReId[v[0]].append(v[5])
	return leftReStart, leftReEnd, leftReRead, leftReReadSum, leftReId

def right_bound_reader(rightBoundFile):
	rightReStart = dict()
	rightReEnd = dict()
	rightReRead = dict ()
	rightReReadSum = dict ()
	rightReId = dict ()
	for l in open(rightBoundFile):
		v = l.rstrip().split()
		if v[0] not in rightReStart:
			rightReStart[v[0]] = list()
			rightReEnd[v[0]] = list()
			rightReRead[v[0]] = list ()
			rightReReadSum[v[0]] = list ()
			rightReId[v[0]] = list()
		rightReStart[v[0]].append(int(v[1]))
		rightReEnd[v[0]].append(int(v[2]))
		rightReRead[v[0]].append(float(v[3]))
		rightReReadSum[v[0]].append(float(v[4]))
		rightReId[v[0]].append(v[5])
	return rightReStart, rightReEnd, rightReRead, rightReReadSum, rightReId

def get_peak_fragment(chrm, leftBound, leftRead, leftReadSum, leftId, rightBound, rightRead, rightReadSum, rightId, outFile, outFile_unmatchleft):
	fOut = open(outFile, 'w')
	fOut2 = open(outFile_unmatchleft, 'w')
	j=0
	lastj=0
	lastMatchedL=0
	lastMatchedLRead=0
	for i in xrange(0,len(rightBound[chrm])):
		for j in xrange(lastj,len(leftBound[chrm])):
			# if reaches the last left boundary
			if (j == len(leftBound[chrm])-1):
				if (leftBound[chrm][j] < rightBound[chrm][i] and lastMatchedL!=leftBound[chrm][j]):
					fOut.write ("\t".join ([str(s) for s in [chrm, leftBound[chrm][j],rightBound[chrm][i],leftRead[chrm][j],rightRead[chrm][i],leftRead[chrm][j]+rightRead[chrm][i],leftId[chrm][j]+rightId[chrm][i],leftReadSum[chrm][j],rightReadSum[chrm][i],leftReadSum[chrm][j]+rightReadSum[chrm][i]]])+"\n")
					lastMatchedL = leftBound[chrm][j]
					lastMatchedLRead = leftRead[chrm][j]
					break
				else:
					fOut2.write ("\t".join ([str(s) for s in [chrm, leftBound[chrm][j],rightBound[chrm][i],"unm_left"]])+"\n")
				break
			else:	
				# if found the match boundaries; hit the pair!
				if (leftBound[chrm][j] < rightBound[chrm][i]):
					if(leftBound[chrm][j+1] >= rightBound[chrm][i]):
						fOut.write ("\t".join ([str(s) for s in [chrm, leftBound[chrm][j],rightBound[chrm][i],leftRead[chrm][j],rightRead[chrm][i],leftRead[chrm][j]+rightRead[chrm][i],leftId[chrm][j]+rightId[chrm][i],leftReadSum[chrm][j],rightReadSum[chrm][i],leftReadSum[chrm][j]+rightReadSum[chrm][i]]])+"\n")
						lastMatchedL = leftBound[chrm][j]
						lastMatchedLRead = leftRead[chrm][j]
						lastj=j+1
						break
					# not find the match right boundaries, just report the unmatched left boundaries
					else:
						fOut2.write ("\t".join ([str(s) for s in [chrm, leftBound[chrm][j],rightBound[chrm][i],leftRead[chrm][j],rightRead[chrm][i],leftReadSum[chrm][j],rightReadSum[chrm][i],"unm_left"]])+"\n")
				else: # if (leftBound[chrm][j] >= rightBound[chrm][i]):
					fOut2.write ("\t".join ([str(s) for s in [chrm, lastMatchedL,rightBound[chrm][i],lastMatchedLRead, rightRead[chrm][i],rightReadSum[chrm][i],"unm_right"]])+"\n")
					break

	fOut.close()
	



sys.stdout.write( "["+time.strftime("%c")+"] Start identifying the peaks... "+"\n")
sys.stdout.flush()

sys.stdout.write( "["+time.strftime("%c")+"] Reading the left boundaries... "+"\n")
sys.stdout.flush()
start = time.clock()
leftReStart, leftReEnd, leftReRead, leftReReadSum, leftReId = left_bound_reader(leftBoundFile)
sys.stdout.write( "Time reading left boundaries file: "+ str(time.clock()-start)+ " s"+"\n")
sys.stdout.flush()

sys.stdout.write( "["+time.strftime("%c")+"] Reading the right boundaries... "+"\n")
sys.stdout.flush()
start = time.clock()
rightReStart, rightReEnd, rightReRead, rightReReadSum, rightReId = right_bound_reader(rightBoundFile)
sys.stdout.write("Time reading right boundaries files: "+ str(time.clock()-start)+ " s"+"\n")
sys.stdout.flush()

get_peak_fragment(chrm,leftReStart, leftReRead, leftReReadSum, leftReId, rightReStart, rightReRead, rightReReadSum, rightReId, outFile,outFile_unmatchleft)
sys.stdout.write( "["+time.strftime("%c")+"] Complete analysis successfully "+"\n")
sys.stdout.flush()


