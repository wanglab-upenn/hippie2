#!/bin/bash

## run_post_hippie2_steps.sh
## alex amlie-wolf 03/23/17
## a master script to run all the post-HIPPIE2 analysis steps for hi-c enhancer-promoter analysis
if [ $# == 1 ]; then
    CFG_FILE=$1
    source ${CFG_FILE}
    mkdir -p ${BASEDIR}/logs/

    DATESTRING=`date +%m_%d_%y_%R:%S`
    LOGOUT="${BASEDIR}/logs/post_hippie2_${DATESTRING}_stdout.txt"
    LOGERR="${BASEDIR}/logs/post_hippie2_${DATESTRING}_stderr.txt"
    
    echo "-------------------------------------------" > \
    	>(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)
    echo "Aggregating interRS data (aggregate_interRS.sh)" > \
    	>(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)
    echo "-------------------------------------------" > \
    	>(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)
    date > >(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)    
    time ./aggregate_interRS.sh ${BASEDIR} ${MAPPABILITY_ANNOT_FOLDER} "${SORT}" ${HIPPIE_FILES} > \
    	>(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)
    
    echo "-------------------------------------------" > \
    	>(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)
    echo "Filtering restriction fragments for length and mappability (filter_5k_08m.sh)" > \
    	>(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)
    echo "-------------------------------------------" > \
    	>(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)
    date > >(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)
    time ./filter_5k_08m.sh ${BASEDIR} > \
    	>(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)

    echo "-------------------------------------------" > \
    	>(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)
    echo "Normalizing read counts with Rao et al normalization matrix (normalization.sh)"> \
    	>(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)
    echo "-------------------------------------------" > \
    	>(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)
    date > >(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)
    time ./normalization.sh ${BASEDIR} ${NORM_PATH} > \
    	>(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)

    echo "-------------------------------------------" > \
    	>(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)
    echo "Identifying physically interacting regions (hicPeakNorm.sh)" > \
    	>(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)
    echo "-------------------------------------------" > \
    	>(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)
    date > >(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)
    time ./hicPeakNorm.sh ${BASEDIR} ${RE} ${RE_FILE} "${SORT}" > \
    	>(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)

    echo "-------------------------------------------" > \
    	>(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)
    echo "Annotating normalized PIRs with genomic and enhancer features (annotateNorm_enhancers.sh)" > \
    	>(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)
    echo "-------------------------------------------"> \
    	>(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)
    date > >(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)
    ## outdated command:
    # time ./annotateNorm_enhancers.sh ${BASEDIR} ${GEN_PATH} ${BEDTOOLS} ${ENCODE_PATH} ${DNASE_PATH} ${OPENCHROM_PATH} ${CELL} > \
    time ./annotateNorm_enhancers.sh ${BASEDIR} ${GEN_PATH} ${BEDTOOLS} ${EPIGEN_PATH} ${CELL} > \
    	>(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)
    
    echo "-------------------------------------------" > \
    	>(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)
    echo "Annotating motif occurrences in PIRs using factorbook data (motif_occurrence.sh)" > \
    	>(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)
    echo "-------------------------------------------" > \
    	>(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)
    date > >(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)
    time ./motif_occurrence.sh ${BASEDIR} ${BEDTOOLS} ${MOTIFPOS_PATH} > \
    	>(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)

    echo "-------------------------------------------" > \
    	>(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)
    echo "Find interacting PIRs using normalized reads (find_normInteraction.sh)" > \
    	>(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)
    echo "-------------------------------------------" > \
    	>(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)
    date > >(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)
    time ./find_normInteraction.sh ${BASEDIR} "${SORT}" > \
    	>(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)

    echo "-------------------------------------------" > \
    	>(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)
    echo "Annotating normalized interacting PIRs with genomic features (annotateNorm_interactor.sh)" > \
    	>(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)
    echo "-------------------------------------------" > \
    	>(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)
    date > >(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)
    time ./annotateNorm_interactor.sh ${BASEDIR} > >(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)

    echo "-------------------------------------------" > \
    	>(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)
    echo "Annotating interacting PIRs with motif overlaps (annotate_interactor_motif.sh)" > \
    	>(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)
    echo "-------------------------------------------" > \
    	>(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)
    date > >(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)
    time ./annotate_interactor_motif.sh ${BASEDIR} ${MOTIFPOS_PATH} ${HG19_G} > \
    	>(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)

    echo "-------------------------------------------" > \
    	>(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)
    echo "Extracting motif interactions (motif_interaction.sh)" > \
    	>(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)
    echo "-------------------------------------------" > \
    	>(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)
    date > >(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)
    time ./motif_interaction.sh ${BASEDIR} ${MOTIFPOS_PATH} > \
    	>(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)

    echo "-------------------------------------------" > \
    	>(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)
    echo "Counting enhancer-promoter pairs supporting motif interactions (motif_motif_interaction.sh)" > \
    	>(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)
    echo "-------------------------------------------" > \
    	>(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)
    date > >(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)
    time ./motif_motif_interaction.sh ${BASEDIR} > \
    	>(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)

    echo "-------------------------------------------" > \
    	>(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)
    echo "Finding interacting proteins in enhancer-promoter pairs (PPI.sh)" > \
    	>(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)
    echo "-------------------------------------------" > \
    	>(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)
    date > >(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)
    time ./PPI.sh ${BASEDIR} ${BIOGRID_ALL} > >(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)

    echo "-------------------------------------------" > \
    	>(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)
    echo "Comparing protein interactions to biogrid database (biogrid_comparison.sh)" > \
    	>(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)
    echo "-------------------------------------------" > \
    	>(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)
    date > >(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)
    time ./biogrid_comparison.sh ${BASEDIR} > >(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)

    echo "-------------------------------------------" > \
    	>(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)
    echo "Summarizing mapping statistics (summarize_mapping_stats.sh)" > \
    	>(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)
    echo "-------------------------------------------" > \
    	>(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)
    date > >(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)
    time ./summarize_mapping_stats.sh ${BASEDIR} ${CELL} ${HIPPIE_FILES} > \
    	>(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2) 

    echo "-------------------------------------------" > \
    	>(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)
    echo "Summarizing annotation statistics (summarize_PIR_stats.sh)" > \
    	>(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)
    echo "-------------------------------------------" > \
    	>(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)
    date > >(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)
    time ./summarize_PIR_stats.sh ${BASEDIR} ${CELL} ${HIPPIE_FILES} > \
    	>(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2) 
fi

    # OLD annotation code, do not run!
    # echo "-------------------------------------------" > \
    # 	>(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)
    # echo "Annotating normalized PIRs with genomic features (annotateNorm.sh)" > \
    # 	>(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)
    # echo "-------------------------------------------"> \
    # 	>(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)
    # date > >(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)    
    # time ./annotateNorm.sh ${BASEDIR} ${GEN_PATH} ${BEDTOOLS} ${ENCODE_PATH} ${DNASE_PATH} ${OPENCHROM_PATH} ${CELL} > \
    # 	>(tee -a ${LOGOUT}) 2> >(tee -a ${LOGERR} >&2)
