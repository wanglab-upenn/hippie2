#!/bin/sh

## annotateNorm.sh
## alex amlie-wolf adapted from yih-chii hwang
## annotates PIRs with various genomics features

# export GEN_PATH="/home/yihhwang/ET_REF/hg19/database"
# export BEDTOOLS="/home/yihhwang/bin/bedtools2-master/bin/bedtools"
# export ENCODE_PATH="/home/yihhwang/ET_REF/hg19/ENCODE/Jan2011"
# export DNASE_PATH="/home/yihhwang/ET_REF/hg19/ENCODE/DNase"
# export OPENCRHOM_PATH="/home/yihhwang/ET_REF/hg19/ENCODE/OpenChromSynth"
# export CELL="Gm12878"

if [ $# == 7 ]; then
    BASEDIR=$1
    GEN_PATH=$2
    BEDTOOLS=$3
    ENCODE_PATH=$4
    DNASE_PATH=$5
    OPENCHROM_PATH=$6
    CELL=$7

    INDIR=${BASEDIR}/hicPeakNorm/
    OUTDIR=${BASEDIR}/annotateNorm/
    mkdir -p ${OUTDIR}
    
    for i in {1..22} X; do
	echo "Annotating PIRs from chr${i} with gene elements"
	# 5.promoter, 6.gene, 7.exon, 8.intron, 9.miRNA, 10.pseudogene, 11.rnaRepeats, 12.TE, 13.TR, 14.intergenic
	cut -f 1-3,10 ${INDIR}/chr${i}_hic_normPeak.bed | $BEDTOOLS annotate -i - -files \
	    "${GEN_PATH}/refGene_promoter_merged.bed" "${GEN_PATH}/refGene_merged.bed" \
	    "${GEN_PATH}/refGene_exon_merged.bed" "${GEN_PATH}/refGene_intron_merged.bed" \
	    "${GEN_PATH}/miRNA_merged.bed" "${GEN_PATH}/vegaPseudoGene_merged.bed" \
	    "${GEN_PATH}/RNA_repeats_rmsk_merged.bed" "${GEN_PATH}/TE_rmsk_merged.bed" \
	    "${GEN_PATH}/TR_rmsk_merged.bed" "${GEN_PATH}/intergenic.bed" \
	    -names reads promoter gene exon intron miRNA pseudogene RNArepeats TE TR intergenic | \
	    sort -k2,2n > ${OUTDIR}/chr${i}_hic_normPeak_gen.bed

	# summarize annotation overlaps
	awk 'BEGIN{OFS="\t"} NR>1 {
              out="";
	      if($5>0) {out="promoter"}
	      if($7>0) {if(out!="") {out = out";exon"} else {out="exon"}}
	      else if($8>0) {if(out!="") {out = out";intron"} else {out="intron"}}
	      print $1,$2,$3,out;}' ${OUTDIR}/chr${i}_hic_normPeak_gen.bed \
		  > ${OUTDIR}/chr${i}_hic_normPeak_genAnno.bed

	# epigenetics annotation (cell-type specific)
	# 1. chrom
	# 2. start
	# 3. end
	# 4. No. of Reads
	# 5. Open region
	# 6. combSeg enhancer
	# 7. H3K27ac
	# 8. H3K4me1
	# 9. H3K4me2
	# 10. H3K4me3
	# 11. H3K27me3
	# 12. H3K36me3 -- elongation mark
	# 13. Dnase
	# 14. PolII
	# 15. P300
	# 16. CTCF
	# $DNASE_PATH/${CELL}DNase.gz
	echo "Annotating PIRs from chr${i} with functional genomics"
	cut -f 1-3,10 ${INDIR}/chr${i}_hic_normPeak.bed | $BEDTOOLS annotate -i - -files \
	    $OPENCHROM_PATH/${CELL}OpenChrom.gz $ENCODE_PATH/${CELL}CombSegEnh.gz \
	    $ENCODE_PATH/${CELL}H3k27ac.gz $ENCODE_PATH/${CELL}H3k4me1.gz \
	    $ENCODE_PATH/${CELL}H3k4me2.gz $ENCODE_PATH/${CELL}H3k4me3.gz \
	    $ENCODE_PATH/${CELL}H3k27me3.gz $ENCODE_PATH/${CELL}H3k36me3.gz \
	    $DNASE_PATH/${CELL}DNase.gz $ENCODE_PATH/${CELL}Pol2.gz \
	    $ENCODE_PATH/${CELL}P300.gz $ENCODE_PATH/${CELL}Ctcf.gz -names reads openChrm segEnh \
	    H3K27ac H3K4me1 H3K4me2 H3K4me3 H3K27me3 H3K36me3 Dnase PolII P300 CTCF | \
	    sort -k2,2n > ${OUTDIR}/chr${i}_hic_normPeak_epi.bed
	
	# get active enhancer annotation
	awk 'BEGIN{OFS="\t"} NR>1 {
              out="";
              if($5>0 && $7+$8>0 && $10+$11==0) {out="e"}
              if($6>0) {if (out != "") {out = out";combSegE"} else {out="combSegE"}}
              print $1,$2,$3,out;}' ${OUTDIR}/chr${i}_hic_normPeak_epi.bed \
		  > ${OUTDIR}/chr${i}_hic_normPeak_epiAnno.bed

	awk 'BEGIN{OFS="\t"} ARGV[1]==FILENAME { a[$2]=$4;next; }
                             {if ($2 in a) print $1,$2,$3,$4,a[$2]}' \
	     ${OUTDIR}/chr${i}_hic_normPeak_epiAnno.bed \
	     ${OUTDIR}/chr${i}_hic_normPeak_genAnno.bed \
				 > ${OUTDIR}/chr${i}_hic_normPeak_regAnno.bed
    done;
else
    echo "Usage: $0 <data directory> <genetics annotation database path> <bedtools path> <ENCODE path> <DNase data path> <open chromatin path> <cell type>"
fi

# export EPI_PATH="/home/yihhwang/ET_REF/hg19/ENCODE/Broad"
