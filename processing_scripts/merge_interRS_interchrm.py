import sys, re, time
from itertools import izip
import pysam
chrm = sys.argv[1] # chromosome that is working on chr1, chr2, ... ,chrY
interRsFile = sys.argv[2]  # RS interaction file
mappFileL = sys.argv[3]	# upward read mappability file 
mappFileR = sys.argv[4]	# downward read mappability file
gcFileL = sys.argv[5]	# uppward read gcFile
gcFileR = sys.argv[6]	# downward read gcFile
outFile = sys.argv[7]	# output file


def attribute_reader(gc_file, mapp_file):
#chr22 16050000 16050004 0.000000 chr22 16049500 16050000
	resite_start = dict()
	gc_f = open (gc_file)
	mapp_f = open (mapp_file)
	for gc_l, mapp_l in izip(gc_f, mapp_f):
		gc = gc_l.rstrip().split()
		mapp = mapp_l.rstrip().split()
		if gc[0] != mapp[0] or gc[1] != mapp[1] or gc[2]!=mapp[2]:
			sys.stderr.write("Error, two files have different coordinate!\t"+gc_l+"\t"+mapp_l+"\n")
			exit(1)
		if gc[0]+"\t"+gc[1] not in resite_start:
			resite_start[gc[0]+"\t"+gc[1]] = [float(gc[3]),float(mapp[3])] 
		else:
			sys.stderr.write("Error, redudant restriction site in the attribute files!\t" +gc_file+"\t"+mapp_file+"\n")
			exit(1)
	gc_f.close()
	mapp_f.close()
	return resite_start


def interRS_reader( in_file, uAtt, dAtt, out_file):
	#with open(in_file1) as f1, open(in_file2) as f2:
	fin = open(in_file)
	fout = open(out_file, 'w')
	interDict=dict()
# chr9    9332084 9332088 u       chr10   58335616        58335620        u       SRR1658572.4    .       good
	#chrX:100000072  u       chrX:100000645  d       SRR1658572.106472663    fine
	first=1
	preInterId="0"
	count=0
	for line in fin:
		v = line.rstrip().split("\t")
		chrm1,pos1S,pos1E = v[0],v[1],v[2]
		chrm2,pos2S,pos2E = v[4],v[5],v[6]

		if first == 1:
			readId=[]
			dirStatus=[]
			#preInterId=chr1+"\t"+pos1+"\t"+v[1]+"\t"+chr2+"\t"+pos2+"\t"+v[3]
			preInterId = "\t".join([str(s) for s in [chrm1,pos1S,pos1E,v[3],chrm2,pos2S,pos2E,v[7]]])
			readId.append(v[4])
			dirStatus.append(v[5])
			count+=1
			first=0	
			continue;

		interId = "\t".join([str(s) for s in [chrm1,pos1S,pos1E,v[3],chrm2,pos2S,pos2E,v[7]]])
		#interId=chr1+"\t"+pos1+"\t"+v[1]+"\t"+chr2+"\t"+pos2+"\t"+v[3]

		if interId != preInterId:
			preloc=preInterId.split("\t")
			if preloc[3] == "d":
				gc1=dAtt[str(preloc[0])+"\t"+str(preloc[1])][0]
				mapp1=dAtt[str(preloc[0])+"\t"+str(preloc[1])][1]
			elif preloc[3] == "u":
				gc1=uAtt[str(preloc[0])+"\t"+str(preloc[1])][0]
				mapp1=uAtt[str(preloc[0])+"\t"+str(preloc[1])][1]
			else:
				sys.stderr.write("Error, the direction of a read is not determined: "+preInterId +"\n")
				exit(1)

			if preloc[7] == "d":
				gc2=dAtt[str(preloc[4])+"\t"+str(preloc[5])][0]
				mapp2=dAtt[str(preloc[4])+"\t"+str(preloc[5])][1]
			elif preloc[7] == "u":
				gc2=uAtt[str(preloc[4])+"\t"+str(preloc[5])][0]
				mapp2=uAtt[str(preloc[4])+"\t"+str(preloc[5])][1]
			else:
				sys.stderr.write("Error, the direction of a read is not determined: "+preInterId +"\n")
				exit(1)

			fout.write(preInterId+"\t"+str(count)+"\t-\t"+str(gc1)+"\t"+str(mapp1)+"\t"+str(gc2)+"\t"+str(mapp2)+"\t"+';'.join(map(str, readId))+"\t"+';'.join(map(str,dirStatus))+"\n")
			readId=[]
			dirStatus=[]
			count=0

		readId.append(v[4])
		dirStatus.append(v[5])
		count+=1
			
		preInterId=interId
	preloc=preInterId.split("\t")
	if preloc[3] == "d":
		gc1=dAtt[str(preloc[0])+"\t"+str(preloc[1])][0]
		mapp1=dAtt[str(preloc[0])+"\t"+str(preloc[1])][1]
	elif preloc[3] == "u":
		gc1=uAtt[str(preloc[0])+"\t"+str(preloc[1])][0]
		mapp1=uAtt[str(preloc[0])+"\t"+str(preloc[1])][1]
	else:
		sys.stderr.write("Error, the direction of a read is not determined: "+preInterId +"\n")
		exit(1)

	if preloc[7] == "d":
		gc2=dAtt[str(preloc[4])+"\t"+str(preloc[5])][0]
		mapp2=dAtt[str(preloc[4])+"\t"+str(preloc[5])][1]
	elif preloc[7] == "u":
		gc2=uAtt[str(preloc[4])+"\t"+str(preloc[5])][0]
		mapp2=uAtt[str(preloc[4])+"\t"+str(preloc[5])][1]
	else:
		sys.stderr.write("Error, the direction of a read is not determined: "+preInterId +"\n")
		exit(1)
	fout.write(preInterId+"\t"+str(count)+"\t-\t"+str(gc1)+"\t"+str(mapp1)+"\t"+str(gc2)+"\t"+str(mapp2)+"\t"+';'.join(map(str, readId))+"\t"+';'.join(map(str,dirStatus))+"\n")
	fin.close()
	fout.close()
	
sys.stdout.write("["+time.strftime("%c")+"] Start merging RS interaction file... " +"\n")
sys.stdout.flush()

sys.stdout.write("["+time.strftime("%c")+"] Reading upward RS gc content file... " + "\n")
sys.stdout.flush()
start = time.clock()
gcLAtt = attribute_reader( gcFileL, mappFileL)
sys.stdout.write("Time reading upward RS gc content file: "+ str(time.clock()-start)+ " s"+"\n")
sys.stdout.flush()

sys.stdout.write("["+time.strftime("%c")+"] Reading downward RS gc content file... " + "\n")
sys.stdout.flush()
start = time.clock()
gcRAtt = attribute_reader( gcFileR, mappFileR)
sys.stdout.write("Time reading downward RS gc content file: "+ str(time.clock()-start)+ " s"+"\n")
sys.stdout.flush()

sys.stdout.write("["+time.strftime("%c")+"] Merging interRS file... " + "\n")
sys.stdout.flush()
start = time.clock()
interRS_reader(interRsFile,gcLAtt, gcRAtt, outFile)
sys.stdout.write("Time merging interRS file: "+ str(time.clock()-start)+ " s"+"\n")
sys.stdout.flush()

sys.stdout.write( "["+time.strftime("%c")+"] Complete analysis (merging_RSinter) successfully "+"\n")
sys.stdout.flush()

