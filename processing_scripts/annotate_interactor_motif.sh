#!/bin/sh

## annotate_interactor_motif.sh
## alex amlie-wolf adapted from yih-chii hwang
## overlaps interactors in PIR-PIR pairs and overlaps them with motifs

# export MOTIFPOS_PATH="/home/yihhwang/analysis/encode_tfbs"
# export HG19_G="/home/yihhwang/ET_REF/hg19/database/human.hg19.genome.chr1-x"

if [ $# == 3 ]; then
    BASEDIR=$1
    MOTIFPOS_PATH=$2
    HG19_G=$3

    OUTDIR=${BASEDIR}/annotate_interactor_motif/
    mkdir -p ${OUTDIR}
    
    echo "Defining genome and background lengths"
    export OVER4KPEAK=`cat ${BASEDIR}/hicPeakNorm/chr*_hic_normPeak.bed | awk 'BEGIN{sum=0}{if ($3-$2>4000){sum+=$3-$2;}}END{print sum}'`
    export GENOME_LEN=`awk 'BEGIN{sum=0}{sum+=$2}END{print sum}' $HG19_G`
    export BG_LEN=`echo $(($GENOME_LEN-$OVER4KPEAK))`

    ## set up the motif occurrence matrix
    echo "Defining motif occurence count matrix"
    motif_occur=($(zcat  ${MOTIFPOS_PATH}/factorbookMotifPos.bed.gz | cut -f4 | sort | uniq -c \
	| awk 'BEGIN{OFS="\t"}{print $2,$1}'))

    declare -A map
    tLen=${#motif_occur[@]}
    for (( c=0; c<${tLen}; c+=2)); do map[${motif_occur[$c]}]=${motif_occur[${c}+1]}; done

    echo "Assigning motifs to interactors"
    awk 'BEGIN{OFS="\t"}ARGV[1]==FILENAME{a[$1,$2]=$4;next;}{if (($1,$2)in a ) print $0,a[$1,$2]}' \
	${BASEDIR}/motif_occurrence/hic_normPeak_containedMotif.bed \
	${BASEDIR}/annotateNorm_interactor/all_norm_interactor_anno2nd.txt \
	> ${OUTDIR}/all_norm_interactor_anno_motif.txt

    echo -ne "motif\ttotal\tenhancer\tpromoter\texon\tintron\tintergenic\n" \
	> ${OUTDIR}/all_norm_interactor_anno_motifOccurrences.txt
    echo -ne "motif\tenhancer\tpromoter\texon\tintron\tintergenic\n" \
	> ${OUTDIR}/all_norm_interactor_anno_motifExistence.txt

    # Number of interactor and # of interactor with motif
    awk -v bgLen=$BG_LEN 'BEGIN{OFS="\t"}{count[$4]+=1;len[$4]+=$3-$2;}
	    END{print "classNum","NA",count["enhancer"],count["promoter"],count["exon"],count["intron"],count["intergenic"];
	        print "classLen",bgLen,len["enhancer"],len["promoter"],len["exon"],len["intron"],len["intergenic"];
	    }' ${BASEDIR}/annotateNorm_interactor/all_norm_interactor_anno2nd.txt \
		>> ${OUTDIR}/all_norm_interactor_anno_motifOccurrences.txt
    
    awk -v bgLen=$BG_LEN 'BEGIN{OFS="\t"}{count[$4]+=1;len[$4]+=$3-$2;}
	    END{print "numWithMotif","NA",count["enhancer"],count["promoter"],count["exon"],count["intron"],count["intergenic"];
	    print "LenWithMotif",bgLen,len["enhancer"],len["promoter"],len["exon"],len["intron"],len["intergenic"];
    }' ${OUTDIR}/all_norm_interactor_anno_motif.txt \
	>> ${OUTDIR}/all_norm_interactor_anno_motifOccurrences.txt

    awk 'BEGIN{OFS="\t"}{count[$4]+=1;}END{print "classNum",count["enhancer"],count["promoter"],count["exon"],count["intron"],count["intergenic"]}' \
	${BASEDIR}/annotateNorm_interactor/all_norm_interactor_anno2nd.txt \
	>> ${OUTDIR}/all_norm_interactor_anno_motifExistence.txt
    
    awk 'BEGIN{OFS="\t"}{count[$4]+=1;}END{print "classWithMotif",count["enhancer"],count["promoter"],count["exon"],count["intron"],count["intergenic"]}' \
	${OUTDIR}/all_norm_interactor_anno_motif.txt \
	>> ${OUTDIR}/all_norm_interactor_anno_motifExistence.txt

    echo "Counting individual motif occurrences"
    for MOTIF_ID in "${!map[@]}";do
	# Motif occurrences
	awk -v motif_id=${MOTIF_ID} -v motif_num=${map[$MOTIF_ID]} 'BEGIN{OFS="\t"}{split($8, a, ",");for (i in a) {if (a[i]==motif_id) sum[$4]+=1}}END{print motif_id,motif_num,sum["enhancer"],sum["promoter"],sum["exon"],sum["intron"],sum["intergenic"]}' \
	    ${OUTDIR}/all_norm_interactor_anno_motif.txt \
	    >> ${OUTDIR}/all_norm_interactor_anno_motifOccurrences.txt
	
      awk -v motif_id=${MOTIF_ID} 'BEGIN{OFS="\t";sum["enhancer"]=sum["promoter"]=sum["exon"]=sum["intron"]=sum["intergenic"]=0}
	    {split($8, a, ",");for (i in a) {if (a[i]==motif_id) {sum[$4]+=1;next}}}
	    END{print motif_id,sum["enhancer"],sum["promoter"],sum["exon"],sum["intron"],sum["intergenic"]}' \
	    ${OUTDIR}/all_norm_interactor_anno_motif.txt \
		>> ${OUTDIR}/all_norm_interactor_anno_motifExistence.txt
    # denominator
    done
    
fi

