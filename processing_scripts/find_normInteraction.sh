#!/bin/sh

## find_normInteraction.sh
## alex amlie-wolf adapted from yih-chii hwang
## find interacting PIRs using normalized reads

if [ $# == 2 ]; then     
    BASEDIR=$1
    SORT=$2

    ## this one uses a lot of different files so i only define the output
    OUTDIR=${BASEDIR}/find_normInteraction/
    mkdir -p ${OUTDIR}
    
    for i in {1..22} X; do
	echo "Finding interactions on chromosome $i"	
	python2.7 ./find_interaction.py ${BASEDIR}/hicPeakNorm/chr${i}_hic_normPeak.bed \
	    ${OUTDIR}/chr${i}_normOut.txt

	$SORT -k2,2n -k5,5n ${OUTDIR}/chr${i}_normOut.txt > ${OUTDIR}/chr${i}_normOut_sorted.txt

	# chr1    567009  568212  chr1    762682  762686  SRR1658586.72286919
	awk 'BEGIN{first=1;OFS="\t";FS="\t";count=0;presite="";read="";readSum=0;}
             ARGV[1]==FILENAME{ a[$1]=$2; next; }
  	     {if (presite!=$1"\t"$2"\t"$3"\t"$4"\t"$5"\t"$6 && first!=1) {
                 print presite,count,readSum,read,dist;readSum=0;read="";count=0} 
	      if (count==0){read=$7;}else{read=read";"$7;}
	      dist=$5-$3;
	      presite=$1"\t"$2"\t"$3"\t"$4"\t"$5"\t"$6;count+=1;readSum+=a[$7];first=0} 
	     END{print presite,count,readSum,read,dist}' ${BASEDIR}/normalization/chr${i}_read_normalized.txt \
		 ${OUTDIR}/chr${i}_normOut_sorted.txt | $SORT -k10,10n -r \
		 > ${OUTDIR}/chr${i}_normOut_merged.txt;

	echo "Finding significant interactions with fit-hic on chromosome $i"
	R --no-save --argv ${OUTDIR}/chr${i} 2000 < ./fit-hic3.R

	awk 'BEGIN{OFS="\t"}NR>=2{if ($17<=0.05)print}' \
	    ${OUTDIR}/chr${i}_normOut_merged_pvalues.txt \
	    > ${OUTDIR}/chr${i}_normOut_merged_sig.txt
    done;

    # merge all intra-chromosomal significant interaction into one file
    echo "Merging all intra-chromosomal significant interactions into one file"
    cat ${OUTDIR}/*normOut_merged_sig.txt > ${OUTDIR}/hic_normOut_merged_sig.txt
else
    echo "Usage: $0 <data directory>"
fi    
    
#R --no-save --argv chr${i} < readAllpossibleInter.R

#for i in {2..22} X ;
#  do R --no-save --argv chr${i} 2000 < fit-hic.R ;
#done;

#for i in {2..22} X ;
#  do R --no-save --argv chr${i} < readAllpossibleInter.R
#done;

