#!/bin/sh

## motif_occurrence.sh
## alex amlie-wolf adapted from yih-chii hwang
## annotating motif occurrences in PIRs using factorbook data

# export BEDTOOLS="/home/yihhwang/bin/bedtools2-master/bin/bedtools"
# export MOTIFPOS_PATH="/home/yihhwang/analysis/encode_tfbs"

if [ $# == 3 ]; then
    BASEDIR=$1
    BEDTOOLS=$2
    MOTIFPOS_PATH=$3

    ## note that this uses data from hicPeakNorm.sh
    INDIR=${BASEDIR}/hicPeakNorm/
    OUTDIR=${BASEDIR}/motif_occurrence/
    mkdir -p ${OUTDIR}
    
    # get motifs contained in the hi-c peaks
    echo "Combining hi-c peaks"
    
    echo -n "" > ${OUTDIR}/hic_normPeak_l4k_sorted.bed
    for i in {1..22} X; do
	awk 'BEGIN{FS="\t";OFS="\t"}{if ($3-$2<=4000) print $1,$2,$3}' \
	    ${INDIR}/chr${i}_hic_normPeak.bed >> ${OUTDIR}/hic_normPeak_l4k_sorted.bed
    done;

    echo "Overlapping peaks with FactorBook motifs"
    $BEDTOOLS intersect -b ${OUTDIR}/hic_normPeak_l4k_sorted.bed \
    	-a ${MOTIFPOS_PATH}/factorbookMotifPos.bed.gz -sorted -f 1.0 -wb | \
    	awk 'BEGIN{OFS="\t"}{print $7,$8,$9,$4,$5,$6}' | \
    	$BEDTOOLS merge -i - -c 4,4 -o collapse,count \
    	> ${OUTDIR}/hic_normPeak_containedMotif.bed
else
    echo "Usage: $0 <data directory> <bedtools path> <motif position path>"
fi
