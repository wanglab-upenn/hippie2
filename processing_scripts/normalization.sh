#!/bin/sh

## normalization.sh
## alex amlie-wolf adapted from yih-chii hwang
## normalized read counts using normalization matrix from Rao et al

if [ $# == 2 ]; then
    BASEDIR=$1
    INDIR=${BASEDIR}/filter_5k_08m/
    OUTDIR=${BASEDIR}/normalization/
    mkdir -p ${OUTDIR}
    
    NORM_PATH=$2    
    
    for i in {1..22} X; do
	echo "Normalizing chr${i}"
	
	awk 'BEGIN{FS="\t";OFS="\t"} ARGV[1]==FILENAME{
                if($1=="NaN") {coef=1} else {coef=$1}; a[NR]=coef;next;}
                {print $0, $9/a[int($2/1000)+1]*a[int($6/1000)+1]}' \
                    "${NORM_PATH}/chr${i}/MAPQG0/chr${i}_1kb.KRnorm" \
                    "${INDIR}/chr${i}_interRS_d5kM08filtered.txt" \
                    > "${OUTDIR}/chr${i}_interRS_normalized.txt"
    done

    for i in {1..22} X; do
    	echo "Read normalizing chr${i}"
    	awk 'BEGIN{FS="\t";OFS="\t"}{split($15, a, ";"); for (x in a) {print a[x],$18/$9}}' \
    	    "${OUTDIR}/chr${i}_interRS_normalized.txt" > "${OUTDIR}/chr${i}_read_normalized.txt"
    done;
else
    echo "Usage: $0 <base directory of analysis> <folder with normalization matrix>"
fi
