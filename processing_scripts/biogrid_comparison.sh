#!/bin/sh

## biogrid_comparison.sh
## alex amlie-wolf adapted from yih-chii hwang
## compares motif-motif interaction data with biogrid annotations

if [ $# == 1 ]; then
    BASEDIR=$1
    OUTDIR=${BASEDIR}/BIOGRID_PPI/

    tail -n +2 ${OUTDIR}/mmep.txt.biogrid.significant | cut -f 1,2 | \
	awk '{print $1;print $2}'| sort -u > ${OUTDIR}/signif_biogrid_TFs.txt

    ## get all the significant interactions involving TFs observed in biogrid
    awk 'BEGIN{FS="\t";OFS="\t"}ARGV[1]==FILENAME{a[$1]=$1;next;} \
         {if($1 in a && $2 in a && $5 <=0.05)print;}' \
	${OUTDIR}/signif_biogrid_TFs.txt ${OUTDIR}/mmep.txt > \
	${OUTDIR}/mmep.signif_biogrid_TF_interactions.txt

    ## now get all the interactions from biogrid involving those TFs
    awk 'BEGIN{FS="\t";OFS="\t";c=0}ARGV[1]==FILENAME{a[$1]=$1;next;} \
         {nA=split(toupper($3),A,"|"); nB=split(toupper($4),B,"|"); \
          A[nA+1]=toupper($1);B[nB+1]=toupper($2); \
          for (x in A) {for (y in B) {if(A[x] in a && B[y] in a) {print}}}}' \
	${OUTDIR}/signif_biogrid_TFs.txt ${OUTDIR}/BIOGRID_SYMBOL.txt > \
	${OUTDIR}/biogrid.signif_biogrid_TF_interactions.txt

    OVERLAP=`tail -n +2 ${OUTDIR}/mmep.txt.biogrid.significant | sort -k1,1 -k2,2 -u | wc -l`
    MMEP=`sort -u ${OUTDIR}/mmep.signif_biogrid_TF_interactions.txt | wc -l`
    BIOGRID=`sort -u ${OUTDIR}/biogrid.signif_biogrid_TF_interactions.txt | wc -l`

    R --no-save --argv $OUTDIR $MMEP $BIOGRID $OVERLAP < ./plot_mmep_bioGrid_venn.R
    echo "Overlap count: $OVERLAP"
    echo "MMEP count: $MMEP"
    echo "BIOGRID count: $BIOGRID"
else
    echo "Usage: $0 <data analysis directory>"
fi
