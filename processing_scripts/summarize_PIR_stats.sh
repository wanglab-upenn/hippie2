#!/bin/sh

## summarize_PIR_stats.sh
if [ $# -ge 2 ]; then
    ## save the directory for the combined analysis output folder
    BASEDIR=$1
    shift
    CELL=$1
    shift
    OUTDIR=${BASEDIR}/full_HIPPIE2_summary/
    mkdir -p ${OUTDIR}

    echo "Summarizing PIR characteristics"
    ## now summarize the annotation and interaction results (earlier steps of the post-HIPPIE2
    ## analysis scripts)
    echo -e "condition\ttype\tcount" > ${OUTDIR}/${CELL}_PIR_characteristics.txt
    
    # 1. Number of total PIRs (hicPeakNorm.sh)
    echo "Counting total PIRs"
    TOTAL_PIRS=`wc -l ${BASEDIR}/hicPeakNorm/chr*_hic_normPeak.bed | tail -1 | awk '{print $1}'`
    echo -e "total_PIRs\tall\t${TOTAL_PIRS}" >> ${OUTDIR}/${CELL}_PIR_characteristics.txt
    
    # 2. Counts of PIRs in various types of annotations (annotateNorm_enhancers.sh)
    echo "Counting PIR overlap with annotations"
    awk -F$'\t' 'BEGIN{OFS=FS} {ANNOT_COUNT[$4]++} END{
         for (annot in ANNOT_COUNT) {{if(annot=="") {annot_out="no_genic_overlap"} else {annot_out=annot}}; print "PIR_genic_annot", annot_out, ANNOT_COUNT[annot]}}' ${BASEDIR}/annotateNorm/*genAnno.bed >> ${OUTDIR}/${CELL}_PIR_characteristics.txt 

    awk -F$'\t' 'BEGIN{OFS=FS} {ANNOT_COUNT[$4]++} END{
         for (annot in ANNOT_COUNT) {{if(annot=="") {annot_out="no_enh_overlap"} else {annot_out=annot}}; print "PIR_enh_annot", annot_out, ANNOT_COUNT[annot]}}' ${BASEDIR}/annotateNorm/*epiAnno.bed >> ${OUTDIR}/${CELL}_PIR_characteristics.txt
    
    # 3. TFBS overlap with factorbook (motif_occurrence.sh)
    ## just count number with overlap
    echo "Counting PIRs overlapping FactorBook"
    TOTAL_MOTIF_PIRS=`wc -l ${BASEDIR}/motif_occurrence/hic_normPeak_containedMotif.bed | awk '{print $1}'`
    echo -e "total_motif_overlapping_PIRs\tall\t${TOTAL_MOTIF_PIRS}" >> ${OUTDIR}/${CELL}_PIR_characteristics.txt
    
    # 4. Counts of interacting PIRs (find_normInteraction.sh)
    ## first count all the interacting pairs across all chromosomes
    echo "Counting interacting PIRs"
    TOTAL_INTERACTING_PIRS=`wc -l ${BASEDIR}/find_normInteraction/*merged_pvalues.txt | tail -1 | awk '{print $1}'`
    ## also count the significant intra-chromosomal ones
    SIGNIF_INTERACTING_PIRS=`wc -l ${BASEDIR}/find_normInteraction/hic_normOut_merged_sig.txt | awk '{print $1}'`

    echo -e "all_interacting_PIRs\tall\t${TOTAL_INTERACTING_PIRS}" >> ${OUTDIR}/${CELL}_PIR_characteristics.txt
    echo -e "signif_interacting_PIRs\tall\t${SIGNIF_INTERACTING_PIRS}" >> ${OUTDIR}/${CELL}_PIR_characteristics.txt        
    
    # 5. Annotation and TFBS overlap of interacting PIRs (annotateNorm_interactor.sh and
    # 	annotate_interactor_motif.sh)
    echo "Counting annotation and TFBS overlap of significant interacting PIRs"
    ## just use the anno2nd_list file to summarize annotation overlaps of interacting sites   
    awk -F$'\t' 'BEGIN{OFS=FS} {print "interacting_PIR_annot", $1"-"$2, $3}' ${BASEDIR}/annotateNorm_interactor/all_norm_interaction_anno2nd_list.txt >> ${OUTDIR}/${CELL}_PIR_characteristics.txt

    ## next, use the motif annotation data to find the total number of significant interacting
    ## sites in each class and also the number with motif overlaps
    ## this is the number of significant interactors in each group
    head -2 ${BASEDIR}/annotate_interactor_motif/all_norm_interactor_anno_motifExistence.txt | \
	tail -1 | awk -F$'\t' 'BEGIN{OFS=FS} {print "signif_interactor_annot", "enhancer", $2; print "signif_interactor_annot", "promoter", $3; print "signif_interactor_annot", "exon", $4; print "signif_interactor_annot", "intron", $5; print "signif_interactor_annot", "intergenic", $6;}' \
	>> ${OUTDIR}/${CELL}_PIR_characteristics.txt
    
    ## this is the number of significant interactors with motif overlaps
    head -3 ${BASEDIR}/annotate_interactor_motif/all_norm_interactor_anno_motifExistence.txt | \
	tail -1 | awk -F$'\t' 'BEGIN{OFS=FS} {print "signif_interactor_annot_with_motif", "enhancer", $2; print "signif_interactor_annot_with_motif", "promoter", $3; print "signif_interactor_annot_with_motif", "exon", $4; print "signif_interactor_annot_with_motif", "intron", $5; print "signif_interactor_annot_with_motif", "intergenic", $6;}' \
	>> ${OUTDIR}/${CELL}_PIR_characteristics.txt
    
    # 6. Motif interactions (motif_interaction.sh)
    ## TODO: figure out if this is counting interactions with NA on both sides..
    echo "Counting motif interactions across annotations"
    awk -F$'\t' 'BEGIN{OFS=FS} {print "interacting_PIR_motif_annot", $1"-"$2, $3}' ${BASEDIR}/motif_interaction/motif_contingency_normTable/motif_contingency_normTable.txt >> ${OUTDIR}/${CELL}_PIR_characteristics.txt 
    
    # 7. Enh-promoter pairs with motif interactions (motif_motif_interaction.sh)    
    
    # 8. Other protein interaction stuff?
						    
    
else
    echo "Usage: $0 <combined analysis outdir> <HIPPIE2 folder 1> <HIPPIE2 folder 2> ..."
fi
