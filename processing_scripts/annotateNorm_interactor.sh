#!/bin/bash

## annotateNorm_interactor.sh
## alex amlie-wolf adapted from yih-chii hwang
## takes interacting sites from normalized peaks and annotates them

if [ $# == 1 ]; then
    BASEDIR=$1
    OUTDIR=${BASEDIR}/annotateNorm_interactor/
    mkdir -p ${OUTDIR}

    #chr1    567009  568208                  chr1    121484121       121485140                       29      120915913
    for i in {1..22} X; do
	# annotate interaction
	echo "Annotating interactions on chr${i}"
	awk 'BEGIN{FS="\t";OFS="\t"}
	    ARGV[1]==FILENAME {gen[$2]=$4; epi[$2]=$5; next}
	    {print $1,$2,$3,gen[$2],epi[$2],$4,$5,$6,gen[$5],epi[$5],$7,$8,$9}' \
	    ${BASEDIR}/annotateNorm/chr${i}_hic_normPeak_regAnno.bed \
	    ${BASEDIR}/find_normInteraction/chr${i}_normOut_merged_sig.txt \
		| sort -k1,1 -k2,2n > ${OUTDIR}/chr${i}_normOut_merged_preAnno.txt

	# Prioritize the annotation for the interaction
	# annotate enhancer as promoter touching PIR
	echo "Annotating enhancers as promoter touching PIRs on chr${i}"
	## make sure the enhancer file is empty
	echo -n "" > ${OUTDIR}/chr${i}_enhancer.txt
	
	awk -v i=${i} -v OUTDIR=${OUTDIR} 'BEGIN{FS="\t";OFS="\t"} {
	    anno1=($4==""?"intergenic":$4)
	    anno1=((anno1~/promoter/)?"promoter":anno1);

	    anno2=($9==""?"intergenic":$9)
	    anno2=((anno2~/promoter/)?"promoter":anno2);

	    if (anno1 != "promoter" && anno2 == "promoter" && ($5=="e"||$5=="combSegE"||$5=="e;combSegE"))
	    {anno1="enhancer"; print $1,$2,$3 >> OUTDIR"/chr"i"_enhancer.txt"}
	    if (anno2 != "promoter" && anno1 == "promoter" && ($10=="e"||$10=="combSegE"||$10=="e;combSegE"))
	    {anno2="enhancer"; print $6,$7,$8 >> OUTDIR"/chr"i"_enhancer.txt"}

	    print $1,$2,$3,anno1,$6,$7,$8,anno2,$11,$12,$13 }' \
		${OUTDIR}/chr${i}_normOut_merged_preAnno.txt > \
		${OUTDIR}/chr${i}_interaction_normAnno1st.txt

	sort -k1,1 -k2,2n ${OUTDIR}/chr${i}_enhancer.txt | uniq \
	    > ${OUTDIR}/chr${i}_enhancer_sorted.txt
	
	awk 'BEGIN{FS="\t";OFS="\t"}
 	     ARGV[1]==FILENAME{a[$1"\t"$2"\t"$3]=$1"\t"$2"\t"$3;next;} {
	     if ($1"\t"$2"\t"$3 in a && $4!="promoter"){anno1="enhancer"} else {anno1=$4} 
	     if ($5"\t"$6"\t"$7 in a && $8!="promoter"){anno2="enhancer"} else {anno2=$8}
	     print $1,$2,$3,anno1,$5,$6,$7,anno2,$9,$10,$11 }' \
	  ${OUTDIR}/chr${i}_enhancer_sorted.txt \
	  ${OUTDIR}/chr${i}_interaction_normAnno1st.txt > \
	  ${OUTDIR}/chr${i}_interaction_normAnno2nd.txt

	# Annotate interactor
	echo "Annotating interactors on chr${i}"
	awk 'BEGIN{FS="\t";OFS="\t"} { ind=$1"\t"$2"\t"$3;
		if(!(ind in readA)) {
		   readA[ind]=$9; normReadA[ind]=$10; distA[ind]=$11;
                   degreeA[ind]=1; annoA[ind]=$4 }
		else{ readA[ind]=readA[ind]","$9; normReadA[ind]=normReadA[ind]","$10;
		      distA[ind]=distA[ind]","$11; degreeA[ind]+=1;}
		ind=$5"\t"$6"\t"$7;
		if(!(ind in readA)) {
		   readA[ind]=$9; normReadA[ind]=$10; distA[ind]=$11; degreeA[ind]=1; annoA[ind]=$8}
		else{ readA[ind]=readA[ind]","$9; normReadA[ind]=normReadA[ind]","$10;
		      distA[ind]=distA[ind]","$11; degreeA[ind]+=1;}}
		END{for (i in readA) {print i,annoA[i],readA[i],distA[i],degreeA[i]}}' \
	  ${OUTDIR}/chr${i}_interaction_normAnno2nd.txt > \
	  ${OUTDIR}/chr${i}_norm_interactor_anno2nd.txt
    done;

    echo "Combining all normalized interactions"
    echo -n "" > ${OUTDIR}/all_norm_interaction_anno2nd.txt
    for i in {1..22} X; do
      cat ${OUTDIR}/chr${i}_interaction_normAnno2nd.txt >> \
	  ${OUTDIR}/all_norm_interaction_anno2nd.txt
    done;

    # further analyses
    # classify the annotation into enhancer promoter intron exon and intergenic
    echo "Classifying annotations further and combining"
    awk 'BEGIN{FS="\t";OFS="\t";}
         {array[1]=$4;array[2]=$8; asort(array,array2); print array2[1],array2[2]}' \
      ${OUTDIR}/all_norm_interaction_anno2nd.txt | sort | uniq -c | \
      awk '{OFS="\t";print $2,$3,$1}' | sort -k3,3nr \
	     > ${OUTDIR}/all_norm_interaction_anno2nd_list.txt

    echo -n "" > ${OUTDIR}/all_norm_interactor_anno2nd.txt
    for i in {1..22} X; do
      sort -k1,1 -k2,2n ${OUTDIR}/chr${i}_norm_interactor_anno2nd.txt \
	  >> ${OUTDIR}/all_norm_interactor_anno2nd.txt
    done;

    cut -f4 ${OUTDIR}/all_norm_interactor_anno2nd.txt | sort | uniq -c \
	> ${OUTDIR}/all_norm_interactor_anno2nd_list.txt
else
    echo "Usage: $0 <data directory>"
fi

