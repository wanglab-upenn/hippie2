#!/bin/sh

## PPI.sh
## alex amlie-wolf adapted from yih-chii hwang
## overlaps motif-motif interactions with data from protein-protein interaction databases

# BIOGRID_ALL=/project/wang2a/rep1/out/BIOGRID-ALL-3.4.126.tab2.txt

if [ $# == 2 ]; then
    BASEDIR=$1
    BIOGRID_ALL=$2

    OUTDIR=${BASEDIR}/BIOGRID_PPI/
    mkdir -p ${OUTDIR}
    
    # get the motif interaction significant matrix (mmep)
    echo "Deriving the significant motif interaction matrix"
    R --no-save --argv ${BASEDIR} < ./calculate_motif_motif_interaction_significance.R
    
    # cut the list of symbol interaction
    cut -f 8,9,10,11,13,16 ${BIOGRID_ALL} | awk \
	'BEGIN{FS="\t";OFS="\t"}{if ($5=="physical"&&$6=="9606") print $1,$2,$3,$4}' \
	> ${OUTDIR}/BIOGRID_SYMBOL.txt
    
    # consider all combination of symbol interaction
    # compare biogrid TFs to those found in this dataset (mmep.txt)
    head -1 ${OUTDIR}/mmep.txt > ${OUTDIR}/mmep.txt.biogrid
    awk 'BEGIN{FS="\t";OFS="\t"}
         ARGV[1]==FILENAME{
             nA=split(toupper($3),A,"|"); nB=split(toupper($4),B,"|");
             A[nA+1]=toupper($1);B[nB+1]=toupper($2);
             for (x in A) {for (y in B) {a[A[x]"\t"B[y]]=1;}} next;}
         {gsub(/-ext/,"",$1);gsub(/-ext/,"",$2);
          if (a[toupper($1)"\t"toupper($2)]==1 || a[toupper($2)"\t"toupper($1)]==1) print}' \
	${OUTDIR}/BIOGRID_SYMBOL.txt ${OUTDIR}/mmep.txt >> ${OUTDIR}/mmep.txt.biogrid
       
    # filter out the significant ones
    head -1 ${OUTDIR}/mmep.txt.biogrid > ${OUTDIR}/mmep.txt.biogrid.significant
    ## use the 5th column (bh_padj, corresponding to the BH correction)
    awk 'BEGIN{FS="\t";OFS="\t"}{if($5<=0.05)print}' ${OUTDIR}/mmep.txt.biogrid \
	>> ${OUTDIR}/mmep.txt.biogrid.significant

    # plot the filtered mmep interaction
    echo "Plotting filtered interactions"
    R --no-save --argv ${OUTDIR} < ./plot_motif_motif_interaction.R
else
    echo "Usage: $0 <data directory> <biogrid interaction file>"
fi
