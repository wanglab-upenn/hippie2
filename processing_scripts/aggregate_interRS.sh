#!/bin/sh

## aggregate_interRS.sh
## alex amlie-wolf adapted from yih-chii Hwang
## aggregates hi-c reads toward their restriction sites
## this should be run from an interactive node with arguments to get 16 nodes:
## -n 8 -R "span[hosts=1]"

if [ $# -ge 4 ]; then

    ## read in arguments: output directory, folder with mappability and GC content files, and
    ## the rest of the arguments are the folders to be analyzed. note that the mappabiilty and
    ## GC content files are assumed to have a specific naming convention
    BASEDIR=$1
    shift
    ANNOT_FOLDER=$1
    shift
    SORT=$1
    shift
    
    ## make a special subdirectory for these results
    mkdir -p ${BASEDIR}/aggregate_interRS/
    OUTDIR=${BASEDIR}/aggregate_interRS/
    mkdir -p ${OUTDIR}/logs/

    echo "Combining interRS files"
    for DIR in $*; do
    	cat ${DIR}/out/*_interRS.txt
    done | awk -v OUT="${OUTDIR}" '{if ($1==$5){ print $0 > OUT"/"$1"_interRS.txt";} else {print $0 > OUT"/interChrm_interRS.txt";}}'

    # sort all intra-chromosomal interactions
    for i in {1..22} X Y; do
	## use the -K flag to send them to the background so I can wait later
	## also the number of cores should correspond to the $SORT command
	bsub -J sort_interRS_chr${i} -n 8 -q wang_normal -e ${OUTDIR}/logs/sort_interRS_chr${i}.e%J\
	    -K -o ${OUTDIR}/logs/sort_interRS_chr${i}.o%J -R "span[hosts=1]"\
	    -M 8192 ./sort_interRS.sh $i ${OUTDIR} ${OUTDIR} "${SORT}" &
    done
    
    # sort all interchromosomal interactions
    # YC's code using parallel sort
    $SORT -k1,1 -k4,4 ${OUTDIR}/interChrm_interRS.txt > ${OUTDIR}/interChrm_interRS_sorted.txt
    # # generic sort code (very slow)
    # sort -T /project/wang2a/temp -k1,1 -k4,4 ${OUTDIR}/interChrm_interRS.txt \
    # 	> ${OUTDIR}/interChrm_interRS_sorted.txt

    ## wait for the bsub jobs to finish
    wait
    
    # JOB_STRING="done(sort_interRS_chr1) && done(sort_interRS_chr2) && done(sort_interRS_chr3) && done(sort_interRS_chr4) && done(sort_interRS_chr5) && done(sort_interRS_chr6) && done(sort_interRS_chr7) && done(sort_interRS_chr8) && done(sort_interRS_chr9) && done(sort_interRS_chr10) && done(sort_interRS_chr11) && done(sort_interRS_chr12) && done(sort_interRS_chr13) && done(sort_interRS_chr14) && done(sort_interRS_chr15) && done(sort_interRS_chr16) && done(sort_interRS_chr17) && done(sort_interRS_chr18) && done(sort_interRS_chr19) && done(sort_interRS_chr20) && done(sort_interRS_chr21) && done(sort_interRS_chr22) && done(sort_interRS_chrX) && done(sort_interRS_chrY)"
    # bsub -q wang_normal -w "${JOB_STRING}" 
    
    # aggregate the interRS and extract their mappability and gc content
    echo "Aggregating interRS interactions and extracting mappability and GC content"
    for i in {1..22} X Y; do
	python2.7 ./merge_interRS.py chr${i} ${OUTDIR}/chr${i}_interRS_sorted.txt \
	    ${ANNOT_FOLDER}/MboI_chr${i}_upwardFlanking500_101nt_mappability.bed \
	    ${ANNOT_FOLDER}/MboI_chr${i}_downwardFlanking500_101nt_mappability.bed \
	    ${ANNOT_FOLDER}/MboI_chr${i}_upwardFlanking500_gc.bed \
	    ${ANNOT_FOLDER}/MboI_chr${i}_downwardFlanking500_gc.bed \
	    ${OUTDIR}/chr${i}_interRS_merged.txt
    done

    python2.7 ./merge_interRS_interchrm.py inter ${OUTDIR}/interChrm_interRS_sorted.txt \
	${ANNOT_FOLDER}/MboI_all_upwardFlanking500_101nt_mappability.bed \
	${ANNOT_FOLDER}/MboI_all_downwardFlanking500_101nt_mappability.bed \
	${ANNOT_FOLDER}/MboI_all_upwardFlanking500_gc.bed \
	${ANNOT_FOLDER}/MboI_all_downwardFlanking500_gc.bed \
	${OUTDIR}/interChrm_interRS_merged.txt
else
    echo "Usage: $0 <outdir> <folder with mappability and GC content files> <HIPPIE2 folder 1> <HIPPIE2 folder 2> ..."   
fi
