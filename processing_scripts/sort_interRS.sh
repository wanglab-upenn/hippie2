#!/bin/sh

## sort_interRS.sh
## alex amlie-wolf adapted from yih-chii hwang, 05/16/16
## sorts the interRS files

i=$1
INDIR=$2
OUTDIR=$3
SORT=$4

## YC's code: uses a special sort command that you need the path to
${SORT} -k2,2n -k5,5n  ${INDIR}/chr${i}_interRS.txt > ${OUTDIR}/chr${i}_interRS_sorted.txt

# generic code (slower, no parallel)
# sort -T /project/wang2a/temp -k2,2n -k5,5n ${INDIR}/chr${i}_interRS.txt > ${OUTDIR}/chr${i}_interRS_sorted.txt
