#!/bin/sh

## convert_rao_peaks_to_UCSC_interact.sh
## alex amlie-wolf 1/31/19

if [ $# == 2 ] ; then
    INFILE=$1
    OUTFILE=$2

    ## name is peak_i_left or peak_i_right
    ## score is negative log10(average of 4 FDRs), rounded to nearest integer
    ## value is number of observed reads ($8)
    ## color is 0, shaded by score (needs spectrum setting)
    
    ## first set up the header
    echo -e 'track type=interact name="GM12878 rep1 Rao et al Interactions" description="GM12878 rep1 Rao Interactions" useScore=on maxHeightPixels=50:100:200 visibility=full' > ${OUTFILE}         
    awk -F$'\t' 'BEGIN{OFS=FS; COUNT=1} {AVG_FDR=-log(($13+$14+$15+$16)/4)/log(10); if(AVG_FDR=="inf") {AVG_FDR=0}; print "chr"$1, $2, $6, "Rao_"COUNT, int(AVG_FDR), $8, ".", 0, "chr"$1, $2, $3, "Rao_"COUNT"_left", ".", "chr"$4, $5, $6, "Rao_"COUNT"_right", "."; COUNT+=1}' \
	${INFILE} >> ${OUTFILE}
elif [ $# == 3 ] ; then
    INFILE=$1
    OUTFILE=$2
    CELL=$3

    ## name is peak_i_left or peak_i_right
    ## score is negative log10(average of 4 FDRs), rounded to nearest integer
    ## value is number of observed reads ($8)
    ## color is 0, shaded by score (needs spectrum setting)
    
    ## first set up the header
    echo -e 'track type=interact name="'"${CELL}"' Rao et al Interactions" description="'"${CELL}"' Rao interactions" useScore=on maxHeightPixels=200 visibility=full' > ${OUTFILE}         
    awk -F$'\t' 'BEGIN{OFS=FS; COUNT=1} {AVG_FDR=-log(($13+$14+$15+$16)/4)/log(10); if(AVG_FDR=="inf") {AVG_FDR=0}; print "chr"$1, $2, $6, "Rao_"COUNT, int(AVG_FDR), $8, ".", 0, "chr"$1, $2, $3, "Rao_"COUNT"_left", ".", "chr"$4, $5, $6, "Rao_"COUNT"_right", "."; COUNT+=1}' \
	${INFILE} >> ${OUTFILE}
fi

